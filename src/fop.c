/*
  FOP: Fight or Perish

  Based on Jack Pavelich's "Dandy" ('Thesis of Terror')
  and Atari Games' "Gauntlet"

  by Bill Kendrick <bill@newbreedsoftware.com>
  http://www.newbreedsoftware.com/fop/

  February 25, 2009 - March 15, 2009
*/

#include <SDL.h>
#include "controller.h"
#include "game.h"
#include "resource.h"
#include "scene.h"
#include "title.h"

int main(int argc, char * argv[])
{
  int quit, i;
  Uint32 timestamp, nowtimestamp;
  int deltatime = 1;

  bool arcade = setup(argc, argv);

  /* Initialize start-game settings */

  level = 1;
  active_players = 1;
  plytypebak = 0;
  for (i = 0; i < 4; i++)
  {
    plytype[i] = i;
  }
  scrollx = scrolly = 0;

  quit = 0;
  memset(controllers, 0, sizeof(controller_t) * 4);

  timestamp = SDL_GetTicks();
  while (update(deltatime))
  {
    if (!hasscene())
    {
      welcome = false;
      gamedata_t *data = demonextlevel(NULL);
      addscene(gameupdate, gamedraw, gamefree, data, false);
      titledata_t *tdata = titleinit();
      addscene(titleupdate, titledraw, titlefree, tdata, false);
    }
    draw(renderer);
    nowtimestamp = SDL_GetTicks();
    if (nowtimestamp - timestamp < (1000 / FPS))
      SDL_Delay(timestamp + (1000 / FPS) - nowtimestamp);
    timestamp = SDL_GetTicks();
  }

  freeimages();

  if (use_sound)
  {
    freesounds();
  }

  SDL_Quit();

  return(0);
}
