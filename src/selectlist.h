#ifndef __SELECTLIST_H
#define __SELECTLIST_H

#include "scene.h"

typedef void (*selectchoicefunc)(int choice);

typedef struct selectitem_s {
  texture_t *img_text;
  selectchoicefunc choicefunc;
} selectitem_t;

typedef struct selectdata_s {
  int option;
  int enditem;
  int top;
  int maxw;
  selectitem_t selectitem[20];
} selectdata_t;

selectdata_t *selectinit();
void selectadd(selectdata_t *data, const char *text, selectchoicefunc choicefunc);
void selectfree(void *data);
bool selectupdate(int deltatime, bool top, void *data);
void selectdraw(void *data);

#endif
