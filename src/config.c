#include <stdio.h>
#include <string.h>
#include <SDL.h>
#include "config.h"
#include "path.h"

int key_codes[4][NUM_KEYS];

controller_code_t controller_codes[4];

int fullscreen;
int joydeadzone;

const char *keynames[NUM_KEYS] = {"DOWN", "UP", "LEFT", "RIGHT", "FIRE", "BOMB", "START"};

void loadconfig()
{
  defaultconfig();

  if (get_user_path() != NULL)
  {
    readconfig();
  }
}

void defaultconfig()
{
  fullscreen = 1;
  joydeadzone = 5000;

  for (int i = 0; i < 4; i++)
  {
    controller_codes[i].joy = -1;
    controller_codes[i].stick_codes[KEY_DOWN].axis = SDL_CONTROLLER_AXIS_INVALID;
    controller_codes[i].stick_codes[KEY_UP].axis = SDL_CONTROLLER_AXIS_INVALID;
    controller_codes[i].stick_codes[KEY_LEFT].axis = SDL_CONTROLLER_AXIS_INVALID;
    controller_codes[i].stick_codes[KEY_RIGHT].axis = SDL_CONTROLLER_AXIS_INVALID;
    controller_codes[i].stick_codes[KEY_FIRE].axis = SDL_CONTROLLER_AXIS_INVALID;
    controller_codes[i].stick_codes[KEY_BOMB].axis = SDL_CONTROLLER_AXIS_INVALID;
    controller_codes[i].stick_codes[KEY_START].axis = SDL_CONTROLLER_AXIS_INVALID;

    controller_codes[i].stick_codes[KEY_DOWN].btn = SDL_CONTROLLER_BUTTON_DPAD_DOWN;
    controller_codes[i].stick_codes[KEY_UP].btn = SDL_CONTROLLER_BUTTON_DPAD_UP;
    controller_codes[i].stick_codes[KEY_LEFT].btn = SDL_CONTROLLER_BUTTON_DPAD_LEFT;
    controller_codes[i].stick_codes[KEY_RIGHT].btn = SDL_CONTROLLER_BUTTON_DPAD_RIGHT;
    controller_codes[i].stick_codes[KEY_FIRE].btn = SDL_CONTROLLER_BUTTON_A;
    controller_codes[i].stick_codes[KEY_BOMB].btn = SDL_CONTROLLER_BUTTON_B;
    controller_codes[i].stick_codes[KEY_START].btn = SDL_CONTROLLER_BUTTON_START;
  }

  key_codes[0][KEY_DOWN] = SDLK_DOWN;
  key_codes[0][KEY_UP] = SDLK_UP;
  key_codes[0][KEY_LEFT] = SDLK_LEFT;
  key_codes[0][KEY_RIGHT] = SDLK_RIGHT;
  key_codes[0][KEY_FIRE] = SDLK_RCTRL;
  key_codes[0][KEY_BOMB] = SDLK_SLASH;
  key_codes[0][KEY_START] = SDLK_1;

  key_codes[1][KEY_DOWN] = SDLK_s;
  key_codes[1][KEY_UP] = SDLK_w;
  key_codes[1][KEY_LEFT] = SDLK_a;
  key_codes[1][KEY_RIGHT] = SDLK_d;
  key_codes[1][KEY_FIRE] = SDLK_q;
  key_codes[1][KEY_BOMB] = SDLK_e;
  key_codes[1][KEY_START] = SDLK_2;

  key_codes[2][KEY_DOWN] = SDLK_KP_2;
  key_codes[2][KEY_UP] = SDLK_KP_8;
  key_codes[2][KEY_LEFT] = SDLK_KP_4;
  key_codes[2][KEY_RIGHT] = SDLK_KP_6;
  key_codes[2][KEY_FIRE] = SDLK_KP_0;
  key_codes[2][KEY_BOMB] = SDLK_KP_PLUS;
  key_codes[2][KEY_START] = SDLK_3;

  key_codes[3][KEY_DOWN] = SDLK_g;
  key_codes[3][KEY_UP] = SDLK_t;
  key_codes[3][KEY_LEFT] = SDLK_f;
  key_codes[3][KEY_RIGHT] = SDLK_h;
  key_codes[3][KEY_FIRE] = SDLK_r;
  key_codes[3][KEY_BOMB] = SDLK_y;
  key_codes[3][KEY_START] = SDLK_4;
}

void readconfig()
{
  FILE * fi;
  char line[1024], fname[1024];
  void * dummy;
  int ply, want, joy, axis, btn;
  char * valstr;

  snprintf(fname, sizeof(fname), "%s/config", get_user_path());
  fi = fopen(fname, "r");
  if (fi != NULL)
  {
    do
    {
      dummy = fgets(line, sizeof(line), fi);
      if (!feof(fi))
      {
        line[strlen(line) - 1] = '\0';
        if (line[0] == 'P' && line[1] >= '1' && line[1] <= '4' && line[2] == '_')
        {
          ply = line[1] - '1';

          if (strstr(line, "FIRE = ") == line + 3)
            want = KEY_FIRE;
          else if (strstr(line, "BOMB = ") == line + 3)
            want = KEY_BOMB;
          else if (strstr(line, "START = ") == line + 3)
            want = KEY_START;
          else if (strstr(line, "UP = ") == line + 3)
            want = KEY_UP;
          else if (strstr(line, "DOWN = ") == line + 3)
            want = KEY_DOWN;
          else if (strstr(line, "LEFT = ") == line + 3)
            want = KEY_LEFT;
          else if (strstr(line, "RIGHT = ") == line + 3)
            want = KEY_RIGHT;
          else
          {
            fprintf(stderr, "Unknown option: %s\n", line);
            want = -1;
          }

          if (want != -1)
          {
            valstr = strstr(line, " = ") + 3;
            if (strstr(valstr, "KEY_") == valstr)
            {
              key_codes[ply][want] = atoi(valstr + 4);
            }
            else if (strstr(valstr, "JOY") == valstr)
            {
              if (strstr(valstr, "_AXIS") == valstr + 3)
              {
                axis = atoi(valstr + 8);

                if (strstr(valstr, "_POS") >= valstr + 9)
                {
                  controller_codes[ply].stick_codes[want].axis = axis;
                  controller_codes[ply].stick_codes[want].axis_sign = 1;
                  controller_codes[ply].stick_codes[want].btn = SDL_CONTROLLER_BUTTON_INVALID;
                }
                else if (strstr(valstr, "_NEG") >= valstr + 9)
                {
                  controller_codes[ply].stick_codes[want].axis = axis;
                  controller_codes[ply].stick_codes[want].axis_sign = -1;
                  controller_codes[ply].stick_codes[want].btn = SDL_CONTROLLER_BUTTON_INVALID;
                }
                else
                  fprintf(stderr, "Unknown option: %s\n", line);
              }
              else if (strstr(valstr, "_BTN") == valstr + 3)
              {
                btn = atoi(valstr + 7);

                controller_codes[ply].stick_codes[want].btn = btn;
                controller_codes[ply].stick_codes[want].axis = SDL_CONTROLLER_AXIS_INVALID;
              }
              else
              {
                fprintf(stderr, "Unknown option: %s\n", line);
              }
            }
          }
        }
        if (strncmp(line, "FULLSCREEN", 10) == 0)
        {
          valstr = strstr(line, " = ") + 3;
          if (atoi(valstr) == 1)
            fullscreen = 1;
          else
            fullscreen = 0;
        }
        if (strncmp(line, "JOYDEADZONE", 11) == 0)
        {
          valstr = strstr(line, " = ") + 3;
          joydeadzone = atoi(valstr);
        }
      }
    }
    while (!feof(fi));
    fclose(fi);
  }
}

void writeconfig()
{
  FILE * fi;
  char line[1024], fname[1024];
  void * dummy;
  int ply, i;
  char * valstr;

  snprintf(fname, sizeof(fname), "%s/config", get_user_path());
  fi = fopen(fname, "w");
  if (fi != NULL)
  {
    fprintf(fi, "FULLSCREEN = %d\n", fullscreen);
    fprintf(fi, "JOYDEADZONE = %d\n", joydeadzone);
    for (ply = 0; ply < 4; ply++)
    {
      for (i = 0; i < NUM_KEYS; i++)
      {
        fprintf(fi, "P%d_%s = KEY_%d\n", ply + 1, keynames[i], key_codes[ply][i]);
        if (controller_codes[ply].stick_codes[i].btn != SDL_CONTROLLER_BUTTON_INVALID)
          fprintf(fi, "P%d_%s = JOY_BTN%d\n", ply + 1, keynames[i], controller_codes[ply].stick_codes[i].btn);
        else
          fprintf(fi, "P%d_%s = JOY_AXIS_%d_%s\n", ply + 1, keynames[i], controller_codes[ply].stick_codes[i].axis, (controller_codes[ply].stick_codes[i].axis_sign == 1 ? "POS" : "NEG"));
      }
    }
    fclose(fi);
  }
}
