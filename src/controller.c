#include <SDL.h>
#include "controller.h"

demostate_t demostate;
controller_t controllers[4];

extern int option;
extern int active_players;
extern int plyx[4], plyy[4];

int processdemo(controller_t controllers[4], map_t *map)
{
  controllers[0].key_right = 0;
  controllers[0].key_left = 0;
  controllers[0].key_down = 0;
  controllers[0].key_up = 0;
  controllers[0].key_fire = 0;
  controllers[0].key_bomb = 0;
  if (demostate.pos < map->demosize)
  {
    switch(map->democmd[demostate.pos].action)
    {
      case DEMOACTION_MOVE:
      {
        if (plyx[0] < map->democmd[demostate.pos].x * TILEW * TILE_PRECISION)
          controllers[0].key_right = 1;
        if (plyx[0] > map->democmd[demostate.pos].x * TILEW * TILE_PRECISION)
          controllers[0].key_left = 1;
        if (plyy[0] < map->democmd[demostate.pos].y * TILEH * TILE_PRECISION)
          controllers[0].key_down = 1;
        if (plyy[0] > map->democmd[demostate.pos].y * TILEH * TILE_PRECISION)
          controllers[0].key_up = 1;
        if (plyy[0] == map->democmd[demostate.pos].y * TILEH * TILE_PRECISION &&
          plyx[0] == map->democmd[demostate.pos].x * TILEW * TILE_PRECISION)
        {
          demostate.pos++;
        }
        break;
      }
      case DEMOACTION_FIRE:
        if (plyx[0] < map->democmd[demostate.pos].x * TILEW * TILE_PRECISION)
          controllers[0].key_right = 1;
        if (plyx[0] > map->democmd[demostate.pos].x * TILEW * TILE_PRECISION)
          controllers[0].key_left = 1;
        if (plyy[0] < map->democmd[demostate.pos].y * TILEH * TILE_PRECISION)
          controllers[0].key_down = 1;
        if (plyy[0] > map->democmd[demostate.pos].y * TILEH * TILE_PRECISION)
          controllers[0].key_up = 1;
        controllers[0].key_fire = 1;
        if (demostate.time == 0)
          demostate.time = map->democmd[demostate.pos].t;
        else
        {
          demostate.time--;
          if (demostate.time == 0)
            demostate.pos++;
        }
        break;
      case DEMOACTION_WAIT:
        if (demostate.time == 0)
          demostate.time = map->democmd[demostate.pos].t;
        else
        {
          demostate.time--;
          if (demostate.time == 0)
            demostate.pos++;
        }
        break;
      case DEMOACTION_BOMB:
        controllers[0].key_bomb = 1;
        demostate.pos++;
        break;
      case DEMOACTION_INSTRUCT:
        if (demostate.time == 0)
        {
          demostate.instruction = map->democmd[demostate.pos].t;
          demostate.x = map->democmd[demostate.pos].x;
          demostate.y = map->democmd[demostate.pos].y;
          demostate.time = 80;
        }
        else
        {
          demostate.time--;
          if (demostate.time == 0)
          {
            demostate.pos++;
            demostate.instruction = 0;
          }
        }
        break;
      default:
        break;
    }
  }
  return PROCESS_GOOD;
}

int processevent(controller_t controllers[4], map_t *map)
{
  /* Handle events: */
  int result = PROCESS_GOOD;
  int i, j;
  SDL_Event event;
  SDL_Keycode key;

  // Bomb only presses once until key down arrives again.
  for (i = 0; i < 4; i++)
  {
    controllers[i].key_bomb = 0;
  }
  while (SDL_PollEvent(&event) > 0)
  {
    if (event.type == SDL_QUIT)
    {
      result = PROCESS_QUIT;
    }
    else if (event.type == SDL_KEYDOWN)
    {
      key = event.key.keysym.sym;
      if (key == SDLK_ESCAPE)
      {
        result = PROCESS_BACK;
      }
      else
      {
        for (i = 0; i < 4; i++)
        {
          if (key == key_codes[i][KEY_DOWN])
            controllers[i].key_down = 1;
          else if (key == key_codes[i][KEY_UP])
            controllers[i].key_up = 1;
          else if (key == key_codes[i][KEY_LEFT])
            controllers[i].key_left = 1;
          else if (key == key_codes[i][KEY_RIGHT])
            controllers[i].key_right = 1;
          else if (key == key_codes[i][KEY_FIRE])
            controllers[i].key_fire = 1;
          else if (key == key_codes[i][KEY_BOMB])
            controllers[i].key_bomb = 1;
          else if (key == key_codes[i][KEY_START])
            controllers[i].key_start = 1;
        }
      }
    }
    else if (event.type == SDL_KEYUP)
    {
      key = event.key.keysym.sym;
      for (i = 0; i < 4; i++)
      {
        if (key == key_codes[i][KEY_DOWN])
          controllers[i].key_down = 0;
        else if (key == key_codes[i][KEY_UP])
          controllers[i].key_up = 0;
        else if (key == key_codes[i][KEY_LEFT])
          controllers[i].key_left = 0;
        else if (key == key_codes[i][KEY_RIGHT])
          controllers[i].key_right = 0;
        else if (key == key_codes[i][KEY_FIRE])
          controllers[i].key_fire = 0;
        else if (key == key_codes[i][KEY_START])
          controllers[i].key_start = 0;
        // Bomb not processed here
      }
    }
    else if (event.type == SDL_CONTROLLERAXISMOTION)
    {
      for (i = 0; i < 4; i++)
      {
        for (j = 0; j < NUM_KEYS; j++)
        {
          if (event.jaxis.which == controller_codes[i].joy &&
              event.jaxis.axis == controller_codes[i].stick_codes[j].axis)
          {
            if ((event.jaxis.value > joydeadzone && controller_codes[i].stick_codes[j].axis_sign > 0) ||
                (event.jaxis.value < -1 * joydeadzone && controller_codes[i].stick_codes[j].axis_sign < 0))
            {
              if (j == KEY_UP)
              {
                controllers[i].key_up = 1;
                controllers[i].key_down = 0;
              }
              else if (j == KEY_DOWN)
              {
                controllers[i].key_down = 1;
                controllers[i].key_up = 0;
              }
              else if (j == KEY_LEFT)
              {
                controllers[i].key_left = 1;
                controllers[i].key_right = 0;
              }
              else if (j == KEY_RIGHT)
              {
                controllers[i].key_right = 1;
                controllers[i].key_left = 0;
              }
            }
            else if (abs(event.jaxis.value) <= joydeadzone)
            {
              if (j == KEY_UP)
                controllers[i].key_up = 0;
              else if (j == KEY_DOWN)
                controllers[i].key_down = 0;
              else if (j == KEY_LEFT)
                controllers[i].key_left = 0;
              else if (j == KEY_RIGHT)
                controllers[i].key_right = 0;
            }
          }
        }
      }
    }
    else if (event.type == SDL_CONTROLLERBUTTONDOWN)
    {
      for (i = 0; i < 4; i++)
      {
        for (j = 0; j < NUM_KEYS; j++)
        {
          if (event.jbutton.which == controller_codes[i].joy &&
              event.jbutton.button == controller_codes[i].stick_codes[j].btn)
          {
            if (j == KEY_UP)
              controllers[i].key_up = 1;
            else if (j == KEY_DOWN)
              controllers[i].key_down = 1;
            else if (j == KEY_LEFT)
              controllers[i].key_left = 1;
            else if (j == KEY_RIGHT)
              controllers[i].key_right = 1;
            else if (j == KEY_FIRE)
              controllers[i].key_fire = 1;
            else if (j == KEY_BOMB)
              controllers[i].key_bomb = 1;
            else if (j == KEY_START)
              controllers[i].key_start = 1;
          }
        }
      }
    }
    else if (event.type == SDL_CONTROLLERBUTTONUP)
    {
      for (i = 0; i < 4; i++)
      {
        for (j = 0; j < NUM_KEYS; j++)
        {
          if (event.jbutton.which == controller_codes[i].joy &&
              event.jbutton.button == controller_codes[i].stick_codes[j].btn)
          {
            if (j == KEY_UP)
              controllers[i].key_up = 0;
            else if (j == KEY_DOWN)
              controllers[i].key_down = 0;
            else if (j == KEY_LEFT)
              controllers[i].key_left = 0;
            else if (j == KEY_RIGHT)
              controllers[i].key_right = 0;
            else if (j == KEY_FIRE)
              controllers[i].key_fire = 0;
            else if (j == KEY_START)
              controllers[i].key_start = 0;
            // Bomb not processed here
          }
        }
      }
    }
  }
  if (controllers[0].key_start == 1 && controllers[1].key_start == 1)
    result = PROCESS_QUIT;
  return result;
}
