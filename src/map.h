#ifndef __MAP_H
#define __MAP_H

#define MAX_ENTITIES 1000

#define TILEW 32
#define TILEH 32

#define TILE_PRECISION 16

enum {
  ENTITY_NONE,
  ENTITY_GENERATOR1,
  ENTITY_GENERATOR2,
  ENTITY_GENERATOR3,
  ENTITY_EYEGEN1,
  ENTITY_EYEGEN2,
  ENTITY_EYEGEN3,
  ENTITY_ENEMY1,
  ENTITY_ENEMY2,
  ENTITY_ENEMY3,
  ENTITY_EYE1,
  ENTITY_EYE2,
  ENTITY_EYE3,
  ENTITY_GRIMREAPER,
  ENTITY_BLAST
};

typedef struct entity_s {
  int type;
  int x, y;
  int w, h;
  int speedx,speedy;
  int anim;
  int cooldown;
  int data;
} entity_t;

typedef struct demo_s {
  int action;
  int x, y, t;
} demo_t;

typedef struct map_s {
  int floortype, walltype;
  int w, h;
  char * m;
  unsigned char * floor;
  unsigned char * wall;
  int startx[4], starty[4];
  entity_t entities[MAX_ENTITIES];
  int demosize;
  demo_t * democmd;
} map_t;

#define setmap(M, y, x, c) (M)->m[((y) * (M)->w) + (x)] = (c)
#define setwall(M, y, x, c) (M)->wall[((y) * (M)->w) + (x)] = (c)
#define getmap(M, y, x) ((M)->m[((y) * (M)->w) + (x)])
#define getfloor(M, y, x) ((M)->floor[((y) * (M)->w) + (x)])
#define getwall(M, y, x) ((M)->wall[((y) * (M)->w) + (x)])

void processentities(map_t *map);
void processwalls(map_t *map);
map_t * loadmap(int level);
void freemap(map_t * * map);
void writemap(int level, map_t *map);

#endif
