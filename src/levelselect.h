#ifndef __LEVELSELECT_H
#define __LEVELSELECT_H

typedef void (*setlevelfunc)(int level);

typedef struct levelselectdata_s {
  int prevfire[4];
  int prevdir[4][2];
  setlevelfunc setlevel;
  int number;
} levelselectdata_t;

void *levelselectinit(setlevelfunc sl);
void levelselectfree(void *data);
bool levelselectupdate(int deltatime, bool top, void *data);
void levelselectdraw(void *data);

#endif
