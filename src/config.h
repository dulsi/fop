#ifndef __CONFIG_H
#define __CONFIG_H

enum {
  KEY_DOWN,
  KEY_UP,
  KEY_LEFT,
  KEY_RIGHT,
  KEY_FIRE,
  KEY_BOMB,
  KEY_START,
  NUM_KEYS
};

extern int key_codes[4][NUM_KEYS];

typedef struct stick_code_s {
  int axis, axis_sign, btn;
} stick_code_t;

typedef struct controller_code_s {
  int joy;
  stick_code_t stick_codes[NUM_KEYS];
} controller_code_t;

extern controller_code_t controller_codes[4];

extern int fullscreen;
extern int joydeadzone;

extern void loadconfig();
extern void defaultconfig();
extern void readconfig();
extern void writeconfig();

#endif
