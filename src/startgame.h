#ifndef __STARTGAME_H
#define __STARTGAME_H

typedef struct startdata_s {
  int ready;
} startdata_t;

void *startinit();
void startfree(void *data);
bool startupdate(int deltatime, bool top, void *data);
void startdraw(void *data);

#endif
