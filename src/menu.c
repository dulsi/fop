#include <SDL.h>
#include "controller.h"
#include "game.h"
#include "resource.h"
#include "menu.h"

menudata_t *menuinit()
{
  menudata_t *data = (menudata_t*)malloc(sizeof(menudata_t));
  memset(data, 0, sizeof(menudata_t));
  return data;
}

void menuadd(menudata_t *data, const char *text, updatefunc u, drawfunc d, freedatafunc freedata, sceneinitfunc sceneinit, addorreplacescene state)
{
  SDL_Color c;
  c.r = 0;
  c.g = 0;
  c.b = 0;
  c.a = 255;
  data->menuitem[data->enditem].img_text = createtext(renderer, text, c);
  data->menuitem[data->enditem].upd = u;
  data->menuitem[data->enditem].drw = d;
  data->menuitem[data->enditem].freedata = freedata;
  data->menuitem[data->enditem].sceneinit = sceneinit;
  data->menuitem[data->enditem].state = state;
  if (data->maxw < data->menuitem[data->enditem].img_text->w)
    data->maxw = data->menuitem[data->enditem].img_text->w;
  data->enditem++;
}

void menufree(void *data)
{
  menudata_t *mdata = (menudata_t*)data;
  for (int i = 0; i < mdata->enditem; i++)
    freeimage(mdata->menuitem[i].img_text);
  free(mdata);
}

bool menuupdate(int deltatime, bool top, void *data)
{
  int oldkey_up[4], oldkey_down[4], oldkey_fire[4];
  int i;
  menudata_t *mdata = (menudata_t*)data;
  for (i = 0; i < 4; i++)
  {
    oldkey_fire[i] = controllers[i].key_fire;
    oldkey_up[i] = controllers[i].key_up;
    oldkey_down[i] = controllers[i].key_down;
  }
  int result = processevent(controllers, NULL);
  if (result == PROCESS_QUIT)
    return false;
  if (result == PROCESS_BACK)
  {
    replacescene(NULL, NULL, NULL, NULL);
    return true;
  }
  for (i = 0; i < 4; i++)
  {
    if (controllers[i].key_down && controllers[i].key_down != oldkey_down[i])
    {
      mdata->option++;
      if (mdata->option >= mdata->enditem)
        mdata->option = 0;
    }
    if (controllers[i].key_up && controllers[i].key_up != oldkey_up[i])
    {
      mdata->option--;
      if (mdata->option < 0)
        mdata->option = mdata->enditem - 1;
    }
    if (controllers[i].key_fire && controllers[i].key_fire != oldkey_fire[i])
    {
      if (mdata->menuitem[mdata->option].upd == NULL)
      {
        if (mdata->menuitem[mdata->option].state == SCENE_REPLACE)
          return false;
        else
          replacescene(NULL, NULL, NULL, NULL);
      }
      else if (mdata->menuitem[mdata->option].state == SCENE_REPLACE)
        replacescene(mdata->menuitem[mdata->option].upd, mdata->menuitem[mdata->option].drw, mdata->menuitem[mdata->option].freedata, (*mdata->menuitem[mdata->option].sceneinit)());
      else if (mdata->menuitem[mdata->option].state == SCENE_REPLACELAST)
        replacelast(mdata->menuitem[mdata->option].upd, mdata->menuitem[mdata->option].drw, mdata->menuitem[mdata->option].freedata, (*mdata->menuitem[mdata->option].sceneinit)());
      else
        addscene(mdata->menuitem[mdata->option].upd, mdata->menuitem[mdata->option].drw, mdata->menuitem[mdata->option].freedata, (*mdata->menuitem[mdata->option].sceneinit)(), false);
    }
    if (controllers[i].key_bomb)
    {
      replacelast(NULL, NULL, NULL, NULL);
    }
  }
  return true;
}

void menudraw(void *data)
{
  SDL_Rect src, dest;
  int i, j;
  menudata_t *mdata = (menudata_t*)data;

  int width = mdata->maxw + 25 + TILEW;
  width = (width / TILEW) * TILEW + (width % TILEW ? TILEW : 0);
  int height = mdata->enditem * 40 + 20;
  height = (height / TILEH) * TILEH + (height % TILEH ? TILEH : 0);

  for (i = 0; i < width; i += TILEW)
  {
    for (j = 0; j < height; j += TILEH)
    {
      if (i == 0 && j == 0)
        src.x = (TILEW + 2) * 5;
      else if (i == width - TILEW && j == 0)
        src.x = (TILEW + 2) * 7;
      else if (j == 0)
        src.x = (TILEW + 2) * 6;
      else if (i == 0 && j == height - TILEH)
        src.x = (TILEW + 2) * 11;
      else if (i == width - TILEW && j == height - TILEH)
        src.x = (TILEW + 2) * 13;
      else if (i == 0)
        src.x = (TILEW + 2) * 8;
      else if (i == width - TILEW)
        src.x = (TILEW + 2) * 10;
      else if (j == height - TILEH)
        src.x = (TILEW + 2) * 12;
      else
        src.x = (TILEW + 2) * 9;
      src.y = (TILEH + 2) * (8);
      src.w = TILEW;
      src.h = TILEH;
      dest.x = (SCREENW - width) / 2 + i;
      dest.y = (SCREENH - height) / 2 + j;
      dest.w = TILEW;
      dest.h = TILEH;
      SDL_RenderCopy(renderer, img_tiles->tex, &src, &dest);
    }
  }

  src.x = 0;
  src.y = 0;
  dest.x = (SCREENW - width) / 2 + 45;
  dest.y = (SCREENH - height) / 2 + 10;
  for (int i = 0; i < mdata->enditem; i++)
  {
    src.w = mdata->menuitem[i].img_text->w;
    src.h = mdata->menuitem[i].img_text->h;

    dest.w = mdata->menuitem[i].img_text->w;
    dest.h = mdata->menuitem[i].img_text->h;

    SDL_RenderCopy(renderer, mdata->menuitem[i].img_text->tex, &src, &dest);
    if (mdata->option == i)
    {
      src.x = 64;
      src.y = 32;
      src.w = TILEW;
      src.h = TILEH;
      dest.x = dest.x - 5 - TILEW;
      dest.w = TILEW;
      dest.h = TILEH;
      SDL_RenderCopy(renderer, img_arrows[2]->tex, &src, &dest);
      src.x = 0;
      src.y = 0;
      dest.x = dest.x + 5 + TILEW;
    }
    dest.y += 40;
  }
}
