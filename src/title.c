#include <SDL.h>
#include "controller.h"
#include "game.h"
#include "resource.h"
#include "scene.h"
#include "startgame.h"
#include "menu.h"
#include "title.h"
#include "about.h"
#include "option.h"
#include "editmap.h"

const char *menu_text[NUM_MENU] = {"Start", "Options", "Edit Map", "About", "Quit"};

titledata_t *titleinit()
{
  titledata_t *data = (titledata_t*)malloc(sizeof(titledata_t));
  memset(data, 0, sizeof(titledata_t));
  return data;
}

void titlefree(void *data)
{
  titledata_t *tdata = (titledata_t*)data;
  free(tdata);
}

bool titleupdate(int deltatime, bool top, void *data)
{
  titledata_t *tdata = (titledata_t*)data;
  if (top)
  {
    int result = processevent(controllers, NULL);
    if ((result == PROCESS_BACK) || (result == PROCESS_QUIT))
      return false;
    for (int i = 0; i < 4; i++)
    {
      if (controllers[i].key_fire)
      {
        if (!arcade)
        {
          menudata_t *menudata = menuinit();
          menuadd(menudata, menu_text[0], startupdate, startdraw, startfree, startinit, SCENE_REPLACE);
          menuadd(menudata, menu_text[1], optionupdate, optiondraw, optionfree, optioninit, SCENE_REPLACE);
          menuadd(menudata, menu_text[2], editmapupdate, editmapdraw, editmapfree, editmapinit, SCENE_REPLACE);
          menuadd(menudata, menu_text[3], aboutupdate, aboutdraw, aboutfree, aboutinit, SCENE_REPLACE);
          menuadd(menudata, menu_text[4], NULL, NULL, NULL, NULL, SCENE_REPLACE);
          addscene(menuupdate, menudraw, menufree, menudata, false);
        }
        else
        {
          active_players = 1 << i;
          replacescene(startupdate, startdraw, startfree, startinit());
        }
      }
      if (controllers[i].key_start)
      {
        active_players = 1 << i;
        replacescene(startupdate, startdraw, startfree, startinit());
      }
    }
  }
  return true;
}

void titledraw(void *data)
{
  drawtitle(scrollx, scrolly);
}
