#ifndef __TITLE_H
#define __TITLE_H

typedef struct titledata_s {
} titledata_t;

titledata_t *titleinit();
void titlefree(void *data);
bool titleupdate(int deltatime, bool top, void *data);
void titledraw(void *data);

#endif
