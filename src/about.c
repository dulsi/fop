#include <SDL.h>
#include "controller.h"
#include "game.h"
#include "resource.h"
#include "about.h"
#include "scene.h"

void *aboutinit()
{
  aboutdata_t *data = (aboutdata_t*)malloc(sizeof(aboutdata_t));
  memset(data, 0, sizeof(aboutdata_t));
  data->img_about = loadimage(renderer, "about.png");
  for (int i = 0; i < 4; i++)
    data->prevfire[i] = 1;
  return data;
}

void aboutfree(void *data)
{
  aboutdata_t *adata = (aboutdata_t*)data;
  free(adata);
}

bool aboutupdate(int deltatime, bool top, void *data)
{
  int i;
  aboutdata_t *adata = (aboutdata_t*)data;

  int result = processevent(controllers, NULL);
  if (result == PROCESS_QUIT)
    return false;
  if (result == PROCESS_BACK)
  {
    replacescene(NULL, NULL, NULL, NULL);
    return true;
  }
  for (i = 0; i < 4; i++)
  {
    if ((controllers[i].key_fire && controllers[i].key_fire != adata->prevfire[i])
      || controllers[i].key_bomb)
    {
      replacescene(NULL, NULL, NULL, NULL);
    }
    adata->prevfire[i] = controllers[i].key_fire;
  }
  return true;
}

void aboutdraw(void *data)
{
  aboutdata_t *adata = (aboutdata_t*)data;
  SDL_RenderCopy(renderer, adata->img_about->tex, NULL, NULL);
}
