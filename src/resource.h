#ifndef __RESOURCE_H
#define __RESOURCE_H

#include <SDL_mixer.h>

typedef struct texture_s {
  SDL_Texture *tex;
  int w, h;
} texture_t;

extern texture_t * img_title, * img_playerselect, * img_stats, * img_numbers;
extern texture_t * img_generators[2], * img_collectibles, * img_badguys[2];
extern texture_t * img_grimreaper, * img_blast;
extern texture_t * img_tiles, * img_check, * img_bigtitle;
extern texture_t * img_player[4], * img_arrows[4];
extern texture_t * img_instructions[4];

extern Mix_Chunk * snd_arrow, * snd_fire, * snd_spell, *snd_pickup, *snd_key;
extern Mix_Chunk * snd_eat, * snd_welcome, * snd_upgrade, *snd_bomb;
extern Mix_Chunk * snd_bombpickup, * snd_transition, * snd_open;
extern Mix_Chunk * snd_lowhealth[4];
extern Mix_Chunk * snd_hurt[4];

extern void loadimages(SDL_Renderer *renderer);
extern void freeimages();
extern texture_t * loadimage(SDL_Renderer *renderer, char * fname);
extern texture_t * createtext(SDL_Renderer *renderer, const char *text, SDL_Color c);
extern void freeimage(texture_t * img);
extern void loadsounds();
extern void loadplayersounds(int ply, int plytype);
extern void freesounds();
extern Mix_Chunk * loadsound(char * fname);

#endif
