#ifndef __CONFIGKEY_H
#define __CONFIGKEY_H

typedef struct configdata_s {
  int ply;
  texture_t *img_configure;
  texture_t *img_playername;
  texture_t **img_keydesc;
  texture_t *img_keyval[NUM_KEYS];
  int where;
} configdata_t;

void *configkeyinit(int ply, texture_t *img_configure, texture_t *img_playername);
void configkeyfree(void *data);
bool configkeyupdate(int deltatime, bool top, void *data);
void configdraw(void *data);

#endif
