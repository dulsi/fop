#include <SDL.h>
#include "controller.h"
#include "game.h"
#include "resource.h"
#include "configkey.h"
#include "scene.h"

void *configkeyinit(int ply, texture_t *img_configure, texture_t *img_playername)
{
  configdata_t *data = (configdata_t*)malloc(sizeof(configdata_t));
  memset(data, 0, sizeof(configdata_t));
  data->ply = ply;
  data->img_configure = img_configure;
  data->img_playername = img_playername;
  data->img_keydesc = loadkeytext();
  SDL_Color c;
  c.r = 255;
  c.g = 255;
  c.b = 255;
  c.a = 255;
  for (int i = 0; i < NUM_KEYS; i++)
  {
    if (key_codes[data->ply][i] != -1)
      data->img_keyval[i] = createtext(renderer, SDL_GetKeyName(key_codes[data->ply][i]), c);
    else
      data->img_keyval[i] = createtext(renderer, "Undefined", c);
  }
  return data;
}

void configkeyfree(void *data)
{
  configdata_t *cdata = (configdata_t*)data;
  for (int i = 0; i < NUM_KEYS; i++)
  {
    freeimage(cdata->img_keydesc[i]);
    freeimage(cdata->img_keyval[i]);
  }
  free(cdata->img_keydesc);
  free(cdata);
}

bool configkeyupdate(int deltatime, bool top, void *data)
{
  int i;
  configdata_t *cdata = (configdata_t*)data;
  SDL_Event event;

  while (SDL_PollEvent(&event) > 0)
  {
    if (event.type == SDL_QUIT)
    {
      return false;
    }
    else if ((event.type == SDL_KEYDOWN) && (cdata->where < NUM_KEYS))
    {
      SDL_Keycode key = event.key.keysym.sym;
      if (key != SDLK_ESCAPE)
      {
        key_codes[cdata->ply][cdata->where] = key;
        for (i = 0; i < 4; i++)
        {
          for (int j = 0; j < NUM_KEYS; j++)
          {
            if ((cdata->ply != i || cdata->where != j) && key_codes[i][j] == key)
            {
              key_codes[i][j] = -1;
            }
          }
        }
        freeimage(cdata->img_keyval[cdata->where]);
        SDL_Color c;
        c.r = 255;
        c.g = 255;
        c.b = 255;
        c.a = 255;
        cdata->img_keyval[cdata->where] = createtext(renderer, SDL_GetKeyName(key), c);
        cdata->where++;
      }
    }
  }
  if (cdata->where >= NUM_KEYS)
    removescene();
  return true;
}

void configdraw(void *data)
{
  int i;
  SDL_Rect dest;
  configdata_t *cdata = (configdata_t*)data;

  SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
  SDL_RenderClear(renderer);
  dest.x = (SCREENW - cdata->img_configure->w - cdata->img_playername->w - 32) / 2;
  dest.y = 0;
  dest.w = cdata->img_configure->w;
  dest.h = cdata->img_configure->h;
  SDL_RenderCopy(renderer, cdata->img_configure->tex, NULL, &dest);
  dest.x += cdata->img_configure->w + 32;
  dest.w = cdata->img_configure->w;
  dest.h = cdata->img_configure->h;
  SDL_RenderCopy(renderer, cdata->img_playername->tex, NULL, &dest);
  for (i = 0; i < NUM_KEYS; i++)
  {
    dest.x = 150;
    dest.y += 40;
    dest.w = cdata->img_keydesc[i]->w;
    dest.h = cdata->img_keydesc[i]->h;
    SDL_RenderCopy(renderer, cdata->img_keydesc[i]->tex, NULL, &dest);
    dest.x = 400;
    dest.w = cdata->img_keyval[i]->w;
    dest.h = cdata->img_keyval[i]->h;
    SDL_RenderCopy(renderer, cdata->img_keyval[i]->tex, NULL, &dest);
  }
}
