#ifndef __SCENE_H
#define __SCENE_H

#include <stdbool.h>
#include <SDL.h>

typedef bool (*updatefunc)(int deltatime, bool top, void *data);
typedef void (*drawfunc)(void *data);
typedef void (*freedatafunc)(void *data);

typedef enum {
  SCENE_ADD,
  SCENE_REPLACE,
  SCENE_REPLACELAST
} addorreplacescene;

typedef struct scene_s
{
  updatefunc upd;
  drawfunc drw;
  freedatafunc freedata;
  void *data;
  bool runonly;
} scene_t;

void replacedata(void *d1, void *d2);
void replacescene(updatefunc u, drawfunc d, freedatafunc freedata, void *data);
void replacelast(updatefunc u, drawfunc d, freedatafunc freedata, void *data);
void addscene(updatefunc u, drawfunc d, freedatafunc freedata, void *data, bool runonly);
void removescene();
bool update(int deltatime);
void draw(SDL_Renderer *renderer);
bool hasscene();

#endif
