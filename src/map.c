#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include "map.h"
#include "path.h"

/* Free a map */

void freemap(map_t * * map)
{
  if ((*map) != NULL)
  {
    if ((*map)->m != NULL)
      free((*map)->m);
    if ((*map)->wall != NULL)
      free((*map)->wall);
    if ((*map)->democmd != NULL)
      free((*map)->democmd);

    free((*map));
    *map = NULL;
  }
}


int calcWalls(map_t *map, char type, int y, int x, bool checkfloor)
{
  int answer = 0;
  int enclosed = 0;
  for (int i = -1; i <= 1; i++)
  {
    for (int j = -1; j <= 1; j++)
    {
      answer = answer << 1;
      enclosed = enclosed << 1;
      if ((y + i < 0) || (x + j < 0) || (y + i >= map->h) || (x + j >= map->w))
      {
        enclosed |= 1;
        continue;
      }
      if (getmap(map, y + i, x + j) == type)
      {
        if ((!checkfloor) || (0 != getfloor(map, y + i, x + j)))
          answer |= 1;
        enclosed |= 1;
      }
    }
  }
  if (enclosed == 0x1FF)
    return enclosed;
  else
    return answer;
}

void processentities(map_t *map)
{
  int currentEntity = 0;

  for (int y = 0; y < map->h; y++)
  {
    for (int x = 0; x < map->w; x++)
    {
      char c = map->m[(y * map->w) + x];
      if (c >= '5' && c <= '8')
      {
        int j = c - '5';
        map->m[(y * map->w) + x] = ' ';

        map->startx[j] = x;
        map->starty[j] = y;
      }
      else if (c >= '1' && c <= '3')
      {
        map->entities[currentEntity].type = ENTITY_GENERATOR1 + c - '1';
        map->entities[currentEntity].x = x * TILEW * TILE_PRECISION;
        map->entities[currentEntity].y = y * TILEH * TILE_PRECISION;
        map->entities[currentEntity].w = TILEW * TILE_PRECISION - 1;
        map->entities[currentEntity].h = TILEH * TILE_PRECISION - 1;
        currentEntity++;
        map->m[(y * map->w) + x] = ' ';
      }
      else if (c >= 'M' && c <= 'O')
      {
        map->entities[currentEntity].type = ENTITY_EYEGEN1 + c - 'M';
        map->entities[currentEntity].x = x * TILEW * TILE_PRECISION;
        map->entities[currentEntity].y = y * TILEH * TILE_PRECISION;
        map->entities[currentEntity].w = TILEW * TILE_PRECISION - 1;
        map->entities[currentEntity].h = TILEH * TILE_PRECISION - 1;
        currentEntity++;
        map->m[(y * map->w) + x] = ' ';
      }
      else if (c >= 'A' && c <= 'F')
      {
        map->entities[currentEntity].type = ENTITY_ENEMY1 + c - 'A';
        map->entities[currentEntity].x = x * TILEW * TILE_PRECISION;
        map->entities[currentEntity].y = y * TILEH * TILE_PRECISION;
        map->entities[currentEntity].w = TILEW * TILE_PRECISION - 1;
        map->entities[currentEntity].h = TILEH * TILE_PRECISION - 1;
        currentEntity++;
        map->m[(y * map->w) + x] = ' ';
      }
      else if (c == '!')
      {
        map->entities[currentEntity].type = ENTITY_GRIMREAPER;
        map->entities[currentEntity].x = x * TILEW * TILE_PRECISION;
        map->entities[currentEntity].y = y * TILEH * TILE_PRECISION;
        map->entities[currentEntity].w = TILEW * TILE_PRECISION - 1;
        map->entities[currentEntity].h = TILEH * TILE_PRECISION - 1;
        currentEntity++;
        map->m[(y * map->w) + x] = ' ';
      }
    }
  }
}

void processwalls(map_t *map)
{
  int x, y;
  int flooroffset;
  int walloffset;

  flooroffset = map->floortype;
  if (flooroffset > 0)
    flooroffset += 120;
  walloffset = map->walltype;
  if (walloffset > 0)
    walloffset = 64 + (walloffset * 4);
  for (y = 0; y < map->h; y++)
  {
    for (x = 0; x < map->w; x++)
    {
      map->floor[(y * map->w) + x] = 9 + flooroffset;
      map->wall[(y * map->w) + x] = 0;
      if (map->m[(y * map->w) + x] == '#')
      {
        int found = calcWalls(map, '#', y, x, false);
        if (found == 0x1FF)
        {
          map->floor[(y * map->w) + x] = 0;
        }
      }
    }
  }
/*  for (y = 0; y < map->h; y++)
  {
    for (x = 0; x < map->w; x++)
    {
      if (getfloor(map, y, x) == 0)
      {
        setmap(map, y, x, ' ');
      }
    }
  }*/
  for (y = 0; y < map->h; y++)
  {
    for (x = 0; x < map->w; x++)
    {
      if (map->m[(y * map->w) + x] == '#')
      {
        int found = calcWalls(map, '#', y, x, true);
        switch (found & 0xBA)
        {
          case 0xBA:
            map->wall[(y * map->w) + x] = 52 + walloffset;
            break;
          case 0x9A:
            map->wall[(y * map->w) + x] = 51 + walloffset;
            break;
          case 0x18:
            map->wall[(y * map->w) + x] = 2 + walloffset;
            break;
          case 0x1A:
            map->wall[(y * map->w) + x] = 20 + walloffset;
            break;
          case 0x30:
            map->wall[(y * map->w) + x] = 4 + walloffset;
            break;
          case 0x12:
            map->wall[(y * map->w) + x] = 3 + walloffset;
            break;
          case 0x32:
            map->wall[(y * map->w) + x] = 33 + walloffset;
            break;
          case 0x3A:
            map->wall[(y * map->w) + x] = 36 + walloffset;
            break;
          case 0x38:
            map->wall[(y * map->w) + x] = 18 + walloffset;
            break;
          case 0x90:
            map->wall[(y * map->w) + x] = 17 + walloffset;
            break;
          case 0x92:
            map->wall[(y * map->w) + x] = 19 + walloffset;
            break;
          case 0xB0:
            map->wall[(y * map->w) + x] = 34 + walloffset;
            break;
          case 0xB2:
            map->wall[(y * map->w) + x] = 49 + walloffset;
            break;
          case 0xB8:
            map->wall[(y * map->w) + x] = 50 + walloffset;
            break;
          case 0x98:
            map->wall[(y * map->w) + x] = 35 + walloffset;
            break;
          default:
            map->wall[(y * map->w) + x] = 1 + walloffset;
            break;
        }
      }
      if (map->m[(y * map->w) + x] == '=')
      {
        int found = calcWalls(map, '=', y, x, false);
        switch (found & 0xBA)
        {
          case 0xBA:
            map->wall[(y * map->w) + x] = 56;
            break;
          case 0x9A:
            map->wall[(y * map->w) + x] = 55;
            break;
          case 0x18:
            map->wall[(y * map->w) + x] = 6;
            break;
          case 0x1A:
            map->wall[(y * map->w) + x] = 24;
            break;
          case 0x30:
            map->wall[(y * map->w) + x] = 8;
            break;
          case 0x12:
            map->wall[(y * map->w) + x] = 7;
            break;
          case 0x32:
            map->wall[(y * map->w) + x] = 37;
            break;
          case 0x3A:
            map->wall[(y * map->w) + x] = 40;
            break;
          case 0x38:
            map->wall[(y * map->w) + x] = 22;
            break;
          case 0x90:
            map->wall[(y * map->w) + x] = 21;
            break;
          case 0x92:
            map->wall[(y * map->w) + x] = 23;
            break;
          case 0xB0:
            map->wall[(y * map->w) + x] = 38;
            break;
          case 0xB2:
            map->wall[(y * map->w) + x] = 53;
            break;
          case 0xB8:
            map->wall[(y * map->w) + x] = 54;
            break;
          case 0x98:
            map->wall[(y * map->w) + x] = 39;
            break;
          default:
            map->wall[(y * map->w) + x] = 5;
            break;
        }
      }
    }
  }
}

/* Load a map */

map_t * loadmap(int level)
{
  char fname[1024], line[1024];
  FILE * fi;
  int x, y, i, j;
  map_t * map;
  void * dummy;

  char *user_path = get_user_path();
  char *data_path = get_data_path();
  snprintf(fname, sizeof(fname), "%s%d.txt", user_path, level);
  fi = fopen(fname, "r");
  if (fi == NULL)
  {
    snprintf(fname, sizeof(fname), "%sdata/maps/%d.txt", data_path, level);
    fi = fopen(fname, "r");
    if (fi == NULL)
      return(NULL);
  }

  map = (map_t *) malloc(sizeof(map_t));
  memset(map->entities, 0, sizeof(map->entities));

  dummy = fgets(line, 1024, fi);
  map->floortype = atoi(line);
  dummy = fgets(line, 1024, fi);
  map->walltype = atoi(line);
  dummy = fgets(line, 1024, fi);
  map->w = atoi(line);
  dummy = fgets(line, 1024, fi);
  map->h = atoi(line);

  for (i = 0; i < 4; i++)
  {
    map->startx[i] = i + 1;
    map->starty[i] = 1;
  }

  map->m = (char *) malloc(sizeof(char) * (map->w * map->h));
  map->floor = (unsigned char *) malloc(sizeof(unsigned char) * (map->w * map->h));
  map->wall = (unsigned char *) malloc(sizeof(unsigned char) * (map->w * map->h));

  for (y = 0; y < map->h; y++)
  {
    dummy = fgets(line, 1024, fi);

    for (x = 0; x < map->w; x++)
    {
      map->m[(y * map->w) + x] = line[x];
    }
  }
  dummy = fgets(line, 1024, fi);
  map->demosize = atoi(line);
  map->democmd = (demo_t *) malloc(sizeof(demo_t) * map->demosize);
  for (i = 0; i < map->demosize; i++)
  {
    dummy = fgets(line, 1024, fi);
    map->democmd[i].action = atoi(line);
    char *nextnum = strchr(line, ' ');
    map->democmd[i].x = atoi(nextnum + 1);
    nextnum = strchr(nextnum + 1, ' ');
    map->democmd[i].y = atoi(nextnum + 1);
    nextnum = strchr(nextnum + 1, ' ');
    map->democmd[i].t = atoi(nextnum + 1);
  }
  fclose(fi);
  processwalls(map);

  return(map);
}

void writemap(int level, map_t *map)
{
  char fname[1024];
  FILE * fi;

  char *user_path = get_user_path();
  snprintf(fname, sizeof(fname), "%s%d.txt", user_path, level);
  fi = fopen(fname, "w");
  if (fi == NULL)
  {
    fprintf(stderr, "Failed to write file %s\n", fname);
    return;
  }
  fprintf(fi, "%d\n", map->floortype);
  fprintf(fi, "%d\n", map->walltype);
  fprintf(fi, "%d\n", map->w);
  fprintf(fi, "%d\n", map->h);
  for (int y = 0; y < map->h; y++)
  {
    for (int x = 0; x < map->w; x++)
    {
      fprintf(fi, "%c", map->m[(y * map->w) + x]);
    }
    fprintf(fi, "\n");
  }
  fclose(fi);
}
