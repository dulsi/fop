#include <SDL.h>
#include "controller.h"
#include "game.h"
#include "resource.h"
#include "startgame.h"
#include "scene.h"

void *startinit()
{
  welcome = false;
  startdata_t *data = (startdata_t*)malloc(sizeof(startdata_t));
  memset(data, 0, sizeof(startdata_t));
  plytype[0] = plytypebak;
  return data;
}

void startfree(void *data)
{
  startdata_t *sdata = (startdata_t*)data;
  free(sdata);
}

bool startupdate(int deltatime, bool top, void *data)
{
  int oldkey_up[4], oldkey_down[4], oldkey_left[4], oldkey_right[4], oldkey_start[4], oldkey_fire[4];
  int i;
  startdata_t *sdata = (startdata_t*)data;

  for (i = 0; i < 4; i++)
  {
    oldkey_up[i] = controllers[i].key_up;
    oldkey_down[i] = controllers[i].key_down;
    oldkey_left[i] = controllers[i].key_left;
    oldkey_right[i] = controllers[i].key_right;
    oldkey_start[i] = controllers[i].key_start;
    oldkey_fire[i] = controllers[i].key_fire;
  }

  int result = processevent(controllers, NULL);
  if (result == PROCESS_QUIT)
    return false;
  if (result == PROCESS_BACK)
  {
    replacescene(NULL, NULL, NULL, NULL);
    return true;
  }
  for (i = 0; i < 4; i++)
  {
    if (controllers[i].key_start && oldkey_start[i] != controllers[i].key_start)
    {
      if ((active_players & (1 << i)) > 0)
        active_players &= ~(1 << i);
      else
        active_players |= 1 << i;
    }
    if (controllers[i].key_fire && oldkey_fire[i] != controllers[i].key_fire)
    {
      if ((sdata->ready & (1 << i)) > 0)
        sdata->ready &= ~(1 << i);
      else
        sdata->ready |= 1 << i;
    }
    else if (controllers[i].key_bomb)
    {
      replacescene(NULL, NULL, NULL, NULL);
      return true;
    }
    if (sdata->ready == 0)
    {
      if (controllers[i].key_up && oldkey_up[i] != controllers[i].key_up)
      {
          level++;
          if (level > 99)
            level = 1;
      }
      if (controllers[i].key_down && oldkey_down[i] != controllers[i].key_down)
      {
          level--;
          if (level < 1)
            level = 99;
      }
    }
    if (controllers[i].key_left && oldkey_left[i] != controllers[i].key_left)
    {
      plytype[i]--;
      if (plytype[i] < 0)
        plytype[i] = 3;
    }
    if (controllers[i].key_right && oldkey_right[i] != controllers[i].key_right)
    {
      plytype[i]++;
      if (plytype[i] > 3)
        plytype[i] = 0;
    }
  }
  if (active_players > 0 && active_players == sdata->ready)
  {
    plytypebak = plytype[0];
    initplayers();
    replacescene(gameupdate, gamedraw, gamefree, gameinit(level, processevent, gamenextlevel));
  }
  return true;
}

void startdraw(void *data)
{
  int i, j, x, y;
  SDL_Rect dest;
  char str[16];
  startdata_t *sdata = (startdata_t*)data;

  /* Draw screen */

  SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
  SDL_RenderClear(renderer);

  /* Title: */
  dest.x = (SCREENW - img_title->w) / 2;
  dest.y = 0;
  dest.w = img_title->w;
  dest.h = img_title->h;
  SDL_RenderCopy(renderer, img_title->tex, NULL, &dest);

  /* Instructions: */
  dest.x = (SCREENW - img_playerselect->w) / 2;
  dest.y = img_title->h;
  dest.w = img_playerselect->w;
  dest.h = img_playerselect->h;
  SDL_RenderCopy(renderer, img_playerselect->tex, NULL, &dest);

  /* Players: */
  for (i = 0; i < 4; i++)
  {
    x = ((SCREENW - TILEW * 8) / 2) + (i * (TILEW * 2));
    y = img_title->h + img_playerselect->h + TILEH;
    dest.x = x;
    dest.y = y;

    drawimage(renderer, img_player[plytype[i]], 6, 12,
            3, 2 + (i * 3),
            dest.x, dest.y);

    if ((active_players & (1 << i)) == 0)
    {
      /* Fade-out effect over disabled players */

      for (j = 0; j < TILEH; j += 2)
      {
        dest.x = x;
        dest.y = y + j;
        dest.w = TILEW;
        dest.h = 1;
        SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
        SDL_RenderFillRect(renderer, &dest);
      }
    } else {
      /* Show player's character stats: */

      for (j = 0; j < char_healths[plytype[i]]; j++)
        drawimage(renderer, img_stats, 4, 1, 1, 1, x + (img_stats->w / 4) * j, y + TILEH);
      for (j = 0; j < char_weapons[plytype[i]]; j++)
        drawimage(renderer, img_stats, 4, 1, 2, 1, x + (img_stats->w / 4) * j, y + TILEH + img_stats->h);
      for (j = 0; j < char_speeds[plytype[i]]; j++)
        drawimage(renderer, img_stats, 4, 1, 3, 1, x + (img_stats->w / 4) * j, y + TILEH + img_stats->h * 2);
      if ((sdata->ready & (1 << i)) != 0)
      {
        drawimage(renderer, img_check, 1, 1, 1, 1, dest.x, dest.y + 100);
      }
    }
  }

  /* Level selection */
  snprintf(str, sizeof(str), "%d", level);
  y = SCREENH - img_numbers->h;
  x = (SCREENW - (img_numbers->w / 10) * strlen(str)) / 2;
  for (i = 0; i < strlen(str); i++)
    drawimage(renderer, img_numbers, 10, 1, str[i] - '0' + 1, 1, x + (i * (img_numbers->w / 10)), y);
}
