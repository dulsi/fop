#ifndef __CONTROLLER_H
#define __CONTROLLER_H

#include "config.h"
#include "map.h"

#define DEMOACTION_MOVE 1
#define DEMOACTION_FIRE 2
#define DEMOACTION_WAIT 3
#define DEMOACTION_BOMB 4
#define DEMOACTION_INSTRUCT 5

typedef struct controller_s {
  int key_up;
  int key_down;
  int key_left;
  int key_right;
  int key_fire;
  int key_bomb;
  int key_start;
} controller_t;

typedef struct demostate_s {
  int pos;
  int time;
  int instruction;
  int x, y;
} demostate_t;

enum {
  PROCESS_GOOD,
  PROCESS_BACK,
  PROCESS_QUIT,
  PROCESS_ENTER
};

extern demostate_t demostate;
extern controller_t controllers[4];

int processdemo(controller_t controllers[4], map_t *map);
int processevent(controller_t controllers[4], map_t *map);

#endif
