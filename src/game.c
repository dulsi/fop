/*
  FOP: Fight or Perish

  Based on Jack Pavelich's "Dandy" ('Thesis of Terror')
  and Atari Games' "Gauntlet"

  by Bill Kendrick <bill@newbreedsoftware.com>
  http://www.newbreedsoftware.com/fop/

  February 25, 2009 - March 15, 2009
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>
#include <SDL.h>
#include <SDL_image.h>
#include <SDL_mixer.h>
#include <SDL_ttf.h>
#include "path.h"
#include "font.h"
#include "config.h"
#include "controller.h"
#include "game.h"
#include "map.h"
#include "resource.h"
#include "scene.h"

#define BLASTW 16
#define BLASTH 16

#define TILE_COLUMNS 16

#define MAXHEALTH 1200

#define DEMOACTION_MOVE 1
#define DEMOACTION_FIRE 2
#define DEMOACTION_WAIT 3
#define DEMOACTION_BOMB 4
#define DEMOACTION_INSTRUCT 5

// Cooldown before player can shoot again.
#define PLAYER_COOLDOWN 4
// Cooldown before hurt sound again.
#define HURT_COOLDOWN 40
// Cooldown before monster can attack again.
#define MELEE_COOLDOWN 8
#define BLAST_COOLDOWN 30

enum {
  DAMAGE_NORMAL,
  DAMAGE_BOMB
};

/* Characters:
  1 - Fast with weak weapon
  2 - Slow but strong
  3 - Evenly balanced
  4 - Weak but with powerful weapon
*/

int entity_speed[1] = {64};
int blast_speed[1] = {128};
int move_speeds[4] = {43, 64, 128, 128};
int char_speeds[4] = {3, 1, 2, 2};
int char_healths[4] = {2, 3, 2, 1};
int char_weapons[4] = {1, 2, 2, 3};
Uint32 char_colors[4] = {0xffff00, 0xff00ff, 0x00ff00, 0x0000ff};

enum {
  UPGRADE_NONE = -1,
  UPGRADE_HEALTH,
  UPGRADE_WEAPON,
  UPGRADE_SPEED,
  NUM_UPGRADES
};
#define UPGRADE_INIT_TIME 200

int use_sound;

SDL_Window * screen;
SDL_Renderer *renderer;

const char *key_text[NUM_KEYS] = {"Down", "Up", "Left", "Right", "Fire", "Bomb", "Start"};

bool welcome;
int active_players;
int plytype[4];
int level;
int plytypebak;
int plyx[4], plyy[4], score[4], health[4], maxhealth[4], bombs[4], keys[4], oldhealth[4], oldscore[4], hurting[4], upgrade[4], upgradetime[4], exited[4];
int plydirx[4], plydiry[4];
bool lowhealth[4];
int plycooldown[4], plymelee[4], plyhurt[4];
int scrollx, scrolly;
bool arcade = false;

int sanitize(int pos, int speed)
{
  int remainder = pos % (TILEH * TILE_PRECISION) % 128 % speed;
  return pos + ((remainder < speed / 2) ? -1 * remainder : speed - remainder);
}

void createentity(map_t *map, int type, int x, int y, int w, int h, int speedx, int speedy, int data)
{
  for (int i = 0; i < MAX_ENTITIES; i++)
  {
    if (map->entities[i].type == ENTITY_NONE)
    {
      map->entities[i].type = type;
      map->entities[i].x = x;
      map->entities[i].y = y;
      map->entities[i].w = w;
      map->entities[i].h = h;
      map->entities[i].speedx = speedx;
      map->entities[i].speedy = speedy;
      map->entities[i].data = data;
      map->entities[i].anim = 0;
      map->entities[i].cooldown = 0;
      return;
    }
  }
}

bool check_collision( SDL_Rect A, SDL_Rect B )
{
    //The sides of the rectangles
    int leftA, leftB;
    int rightA, rightB;
    int topA, topB;
    int bottomA, bottomB;

    //Calculate the sides of rect A
    leftA = A.x;
    rightA = A.x + A.w;
    topA = A.y;
    bottomA = A.y + A.h;

    //Calculate the sides of rect B
    leftB = B.x;
    rightB = B.x + B.w;
    topB = B.y;
    bottomB = B.y + B.h;

     //If any of the sides from A are outside of B
    if( bottomA <= topB )
    {
        return false;
    }

    if( topA >= bottomB )
    {
        return false;
    }

    if( rightA <= leftB )
    {
        return false;
    }

    if( leftA >= rightB )
    {
        return false;
    }

    //If none of the sides from A are outside B
    return true;
}

int collision(map_t *map, int x, int y, int w, int h, int skipct, int *skip)
{
  SDL_Rect first, second;
  first.x = x;
  first.y = y;
  first.w = w;
  first.h = h;
  second.w = TILEW * TILE_PRECISION - 1;
  second.h = TILEH * TILE_PRECISION - 1;
  for (int i = 0; i < MAX_ENTITIES; i++)
  {
    bool found = false;
    for (int j = 0; j < skipct; j++)
    {
      if (i == skip[j])
      {
        found = true;
        break;
      }
    }
    if (found)
      continue;
    if (map->entities[i].type != ENTITY_NONE)
    {
      second.x = map->entities[i].x;
      second.y = map->entities[i].y;
      if (check_collision(first, second))
      {
        return i;
      }
    }
  }
  return -1;
}

gamedata_t *demonextlevel(gamedata_t *gdata)
{
  demostate.pos = 0;
  demostate.time = 0;
  demostate.instruction = 0;
  active_players = 1;
  plytype[0] = 2;
  initplayers();
  return gameinit(0, processdemo, demonextlevel);
}

gamedata_t *gamenextlevel(gamedata_t *gdata)
{
  return gameinit(gdata->level + 1, gdata->processinput, gdata->nextlevel);
}

gamedata_t *gameinit(int level, processinputfunc processinput, nextlevelfunc nextlevel)
{
  int i, j;
  gamedata_t *data = (gamedata_t *)malloc(sizeof(gamedata_t));
  memset(data, 0, sizeof(gamedata_t));
  data->level = level;
  data->map = loadmap(level);
  if (data->map == NULL)
  {
    free(data);
    return NULL;
  }
  processentities(data->map);
  data->processinput = processinput;
  data->nextlevel = nextlevel;
  for (i = 0; i < 4; i++)
  {
    exited[i] = 0;

    /* Start out facing down */
    plydirx[i] = 2;
    plydiry[i] = 3;
    plyx[i] = data->map->startx[i] * TILEW * TILE_PRECISION;
    plyy[i] = data->map->starty[i] * TILEH * TILE_PRECISION;
    plycooldown[i] = 0;
    plymelee[i] = 0;
    plyhurt[i] = 0;

    for (j = 0; j < MAXARROWS; j++)
      data->arrows[i][j].alive = 0;
  }
  return data;
}

void gamefree(void *data)
{
  gamedata_t *gdata = (gamedata_t *)data;
  freemap(&gdata->map);
  free(gdata);
}

bool gameupdate(int deltatime, bool top, void *data)
{
  gamedata_t *gdata = (gamedata_t *)data;
  int minx, maxx, miny, maxy;
  int i, j;
  char c;

  if (!welcome)
  {
    welcome = true;
    Mix_PlayChannel(-1, snd_welcome, 0);
  }

  gdata->toggle++;

  /* Determine center of all live player's positions, to center scrolling: */

  maxx = 0;
  maxy = 0;
  minx = gdata->map->w * TILEW;
  miny = gdata->map->h * TILEH;
  j = 0;
  for (i = 0; i < 4; i++)
  {
    if (health[i] > 0 && exited[i] == 0)
    {
      if ((plyx[i] / TILE_PRECISION) < minx)
        minx = plyx[i] / TILE_PRECISION;
      if ((plyx[i] / TILE_PRECISION) + TILEW > maxx)
        maxx = plyx[i] / TILE_PRECISION + TILEW;

      if ((plyy[i] / TILE_PRECISION) < miny)
        miny = plyy[i] / TILE_PRECISION;
      if ((plyy[i] / TILE_PRECISION) + TILEH > maxy)
        maxy = plyy[i] / TILE_PRECISION + TILEH;

      j++;
    }
  }
  if (j > 0)
  {
    scrollx = ((maxx - minx) / 2) + minx;
    scrolly = ((maxy - miny) / 2) + miny;
    if (scrollx < SCREENW / 2)
      scrollx = SCREENW / 2;
    else if (gdata->map->w * TILEW <= scrollx + SCREENW / 2)
      scrollx = gdata->map->w * TILEW - SCREENW / 2;
    if (scrolly < SCREENH / 2)
      scrolly = SCREENH / 2;
    else if (gdata->map->h * TILEH <= scrolly + SCREENH / 2)
      scrolly = gdata->map->h * TILEH - SCREENH / 2;
  }

  for (i = 0; i < 4; i++)
  {
    /* Temporarily keep track of old state for comparison or rollback: */

    oldhealth[i] = health[i];

    oldscore[i] = score[i];
  }

  /* Handle events: */

  int presult = (*gdata->processinput)(controllers, gdata->map);
  if (presult == PROCESS_QUIT)
  {
    return false;
  }
  if (presult == PROCESS_BACK)
  {
    replacescene(NULL, NULL, NULL, NULL);
    return true;
  }
  for (i = 0; i < 4; i++)
  {
    if (controllers[i].key_bomb)
    {
      if (bombs[i] > 0 && health[i] > 0 && exited[i] == 0)
      {
        score[i] += bomb(gdata->map, scrollx * TILE_PRECISION, scrolly * TILE_PRECISION);
        bombs[i]--;
        Mix_PlayChannel(-1, snd_bomb, 0);
      }
    }
  }

  /* Based on keys, which direction is the player facing? */

  for (i = 0; i < 4; i++)
  {
    updateplayer(deltatime, gdata->toggle, scrollx, scrolly, i, &controllers[i], gdata->arrows[i], gdata->map);
  }

  updateentities(deltatime, gdata->toggle, scrollx, scrolly, gdata->map);

  if ((gdata->toggle & 0xF) == 0x8)
  {
    for (int y = 0; y < gdata->map->h; y++)
    {
      for (int x = 0; x < gdata->map->w; x++)
      {
        c = getmap(gdata->map, y, x);
        if (c == '$' && (rand() % 2) == 1)
          setmap(gdata->map, y, x, '&');
        if (c == '&' && (rand() % 2) == 1)
          setmap(gdata->map, y, x, '$');
      }
    }
  }

  /* Handle players' heath- and timer-related things: */

  for (i = 0; i < 4; i++)
  {
    /* See if a player has earned enough points for a health upgrade */

    if (health[i] > 0 && (oldscore[i] / 1000) < (score[i] / 1000))
    {
      health[i] = maxhealth[i];
      maxhealth[i] += (MAXHEALTH / 4);
      if (maxhealth[i] > MAXHEALTH)
        maxhealth[i] = MAXHEALTH; 
      /* FIXME: Sound effect */
    }


    /* Constantly reduce player health: */

    if (gdata->toggle % 8 == 0 && exited[i] == 0)
      health[i]--;


    /* Are we still alive?! */

    if (health[i] <= 0)
    {
      health[i] = 0;
      if (oldhealth[i] > 0)
      {
        setmap(gdata->map, plyy[i] / TILEH / TILE_PRECISION, plyx[i] / TILEW / TILE_PRECISION, ('w' + i));
        /* FIXME: Sound effect */
      }
    }
    else if (health[i] <= maxhealth[i] / 4 && !lowhealth[i])
    {
      lowhealth[i] = true;
      Mix_PlayChannel(-1, snd_lowhealth[i], 0);
    }
    else if (health[i] > maxhealth[i] /4 && lowhealth[i])
    {
      lowhealth[i] = false;
    }

    if (health[i] < oldhealth[i] - 1)
      hurting[i] = 1;
    else
      hurting[i] = 0;

    if (plyhurt[i] > 0)
    {
      plyhurt[i] -= deltatime;
      if (plyhurt[i] < 0)
        plyhurt[i] = 0;
    }
    if (plyhurt[i] == 0 && hurting[i] == 1)
    {
      Mix_PlayChannel(-1, snd_hurt[i], 0);
      plyhurt[i] = HURT_COOLDOWN;
    }

    /* Reduce upgrade time: */
    if ((upgradetime[i] > 0) && (gdata->toggle % 4 == 0))
    {
      upgradetime[i]--;
      if (upgradetime[i] <= 0)
        upgrade[i] = UPGRADE_NONE;
    }
  }

  /* Determine if anyone is alive, and who has exited */

  int allexited = 1;
  int anyalive = 0;
  for (i = 0; i < 4; i++)
  {
    if (health[i] > 0 && exited[i] == 0)
      allexited = 0;
    anyalive += health[i];
  }

  if (anyalive == 0)
    replacescene(NULL, NULL, NULL, NULL);
  else if (allexited)
  {
    gamedata_t *d2 = (*gdata->nextlevel)(gdata);
    if (d2 == NULL)
      replacescene(NULL, NULL, NULL, NULL);
    else
      replacedata(gdata, d2);
  }
  return true;
}

void gamedraw(void *data)
{
  gamedata_t *gdata = (gamedata_t *)data;
  int anyalive = 0;
  int i;
  for (i = 0; i < 4; i++)
  {
    anyalive += health[i];
  }

  /* Clear the screen */

  if (anyalive > 0)
  {
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
    SDL_RenderClear(renderer);
  }
  else
  {
    SDL_SetRenderDrawColor(renderer, 32, 0, 0, 255);
    SDL_RenderClear(renderer);
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
  }


  /* Draw the objects we can see from here */

  drawtiles(gdata->map, scrollx, scrolly);

  drawentities(gdata->map, scrollx, scrolly);

  /* Draw the players */

  for (i = 0; i < 4; i++)
  {
    drawplayer(gdata->toggle, scrollx, scrolly, &controllers[i], i, gdata->arrows[i]);
  }

  drawhud(gdata->toggle);
}

/* Determine whether player can move into a particular spot */

int safe(map_t * map, int x, int y, int w, int h, int toggle, int *wx, int *wy)
{
  char c, c2;

  if (x < 0 || y < 0 || (x / TILEW / TILE_PRECISION) >= map->w || (y / TILEH / TILE_PRECISION) >= map->h)
    return -1;

  int squarex = x / TILEW / TILE_PRECISION;
  int squarey = y / TILEH / TILE_PRECISION;
  c = getmap(map, squarey, squarex);
  *wx = squarex;
  *wy = squarey;
  if ((c != '#') && (((x + w) / TILE_PRECISION / TILEW != squarex)))
  {
    c2 = getmap(map, squarey, squarex + 1);
    if (c2 == '=' || c2 == '#' || c == ' ')
    {
      c = c2;
      *wx = squarex + 1;
      *wy = squarey;
    }
  }
  if ((c != '#') && ((y + h) / TILE_PRECISION / TILEH != squarey))
  {
    c2 = getmap(map, squarey + 1, squarex);
    if (c2 == '=' || c2 == '#' || c == ' ')
    {
      c = c2;
      *wx = squarex;
      *wy = squarey + 1;
    }
  }
  if ((c != '#') && ((x + w) / TILE_PRECISION / TILEW != squarex) && ((y + h) / TILE_PRECISION / TILEH != squarey))
  {
    c2 = getmap(map, squarey + 1, squarex + 1);
    if (c2 == '=' || c2 == '#' || c == ' ')
    {
      c = c2;
      *wx = squarex + 1;
      *wy = squarey + 1;
    }
  }

  if (c == '#')
  {
    /* Cannot walk through walls: */
    return -1;
  }

  /* Otherwise, go ahead and try moving there */
  /* Note: We may be bounced back (e.g., if you try to walk
     through a door without any keys, or you're melee-attacking
     a bad guy, you'll get bounced back, even though we report
     it as being 'safe' here). */

  return (int) c;
}

/* Explode a bomb - hurt all bad things visible on the screen */

int bomb(map_t * map, int plyx, int plyy)
{
  int score;

  score = 0;

  SDL_Rect first, second;
  first.x = plyx - SCREENW / 2 * TILE_PRECISION;
  first.y = plyy - SCREENH / 2 * TILE_PRECISION;
  first.w = SCREENW * TILE_PRECISION;
  first.h = SCREENH * TILE_PRECISION;
  second.w = TILEW * TILE_PRECISION - 1;
  second.h = TILEH * TILE_PRECISION - 1;
  for (int i = 0; i < MAX_ENTITIES; i++)
  {
    if (map->entities[i].type != ENTITY_NONE)
    {
      second.x = map->entities[i].x;
      second.y = map->entities[i].y;
      if (check_collision(first, second))
      {
        score += entity_damage(&map->entities[i], DAMAGE_BOMB);
      }
    }
  }

  return(score);
}


/* 'Open' a door, by removing all door pieces that touch.
   Note: a recursive function, but uses two x/y coordinates
   and does up/down/left/right adjacency tests to crawl
   along the door and remove it.  This means no more than
   two door pieces should touch on the map, or the door
   won't open right! */

void opendoor(map_t * map, int x, int y)
{
  int x1, y1, x2, y2;
  char c1l, c1r, c1u, c1d;
  char c2l, c2r, c2u, c2d;
  int done;

  /* Start at the spot on the door that the player touched */

  x1 = x2 = x;
  y1 = y2 = y;

  do
  {
    done = 1;

    /* Look around x/y coordinate #1 */

    c1l = getmap(map, y1, x1 - 1);
    c1r = getmap(map, y1, x1 + 1);
    c1u = getmap(map, y1 - 1, x1);
    c1d = getmap(map, y1 + 1, x1);


    /* Remove door at x/y coordinates #1 and #2 */

    setmap(map, y1, x1, ' ');
    setmap(map, y2, x2, ' ');
    setwall(map, y1, x1, 0);
    setwall(map, y2, x2, 0);

    /* Crawl x/y coordinate #1 towards any door piece
       that's adjacent */

    if (c1l == '=')
    {
      done = 0;
      x1--;
    }
    else if (c1r == '=')
    {
      done = 0;
      x1++;
    }
    else if (c1u == '=')
    {
      done = 0;
      y1--;
    }
    else if (c1d == '=')
    {
      done = 0;
      y1++;
    }

    /* If we found a piece, remove it
       (so that x/y coordinate #2 doesn't see it) */

    if (!done)
    {
      setmap(map, y1, x1, ' ');
      setwall(map, y1, x1, 0);
    }


    /* Look around x/y coordinate #1 */

    c2l = getmap(map, y2, x2 - 1);
    c2r = getmap(map, y2, x2 + 1);
    c2u = getmap(map, y2 - 1, x2);
    c2d = getmap(map, y2 + 1, x2);

    /* Crawl x/y coordinate #1 towards any door piece
       that's adjacent */

    if (c2l == '=')
    {
      done = 0;
      x2--;
    }
    else if (c2r == '=')
    {
      done = 0;
      x2++;
    }
    else if (c2u == '=')
    {
      done = 0;
      y2--;
    }
    else if (c2d == '=')
    {
      done = 0;
      y2++;
    }

    /* Note: The piece at x/y coordinate #2 will be removed
       when this loop repeats. */
  }
  while (!done);

  /* Note: Loop stops repeating once all adjacent door pieces have
     been found. */
}

void drawimage(SDL_Renderer * renderer, texture_t * sheet, int tilew, int tileh, int tilex, int tiley, int x, int y)
{
  SDL_Rect src, dest;

  src.x = (sheet->w / tilew) * (tilex - 1);
  src.y = (sheet->h / tileh) * (tiley - 1);
  src.w = (sheet->w / tilew);
  src.h = (sheet->h / tileh);

  dest.x = x;
  dest.y = y;
  dest.w = src.w;
  dest.h = src.h;

  if ((img_player[0] == sheet) || (img_player[1] == sheet) || (img_player[2] == sheet) || (img_player[3] == sheet))
  {
    dest.y -= 8;
    dest.x -= 4;
  }

  SDL_RenderCopy(renderer, sheet->tex, &src, &dest);
}

void drawtile(SDL_Renderer * renderer, texture_t * sheet, int tilesw, int tilesh, int spacer, int tile, int x, int y)
{
  SDL_Rect src, dest;

  src.x = (tilesw + spacer) * (tile % TILE_COLUMNS);
  src.y = (tilesh + spacer) * (tile / TILE_COLUMNS);
  src.w = tilesw;
  src.h = tilesh;

  dest.x = x;
  dest.y = y;
  dest.w = tilesw;
  dest.h = tilesh;

  SDL_RenderCopy(renderer, sheet->tex, &src, &dest);
}

int closestplayer(map_t * map, int plyx[4], int plyy[4], int health[4], int x, int y)
{
  int i, want, dx, dy, thisdist, dist;

  /* Default (in case everyone's dead) */

  want = ((x + y) % 4);
  dist = (map->w * TILEW * TILE_PRECISION) * (map->h * TILEH * TILE_PRECISION);

  for (i = 0; i < 4; i++)
  {
    if (health[i] > 0)
    {
      dx = plyx[i] - x;
      dy = plyy[i] - y;
      thisdist = sqrt((dx * dx) + (dy * dy));
      if (thisdist < dist)
      {
        want = i;
        dist = thisdist;
      }
    }
  }

  return(want);
}

texture_t **loadkeytext()
{
  texture_t **result;
  result = (texture_t **)malloc(sizeof(texture_t *) * NUM_KEYS);
  SDL_Color c;
  c.r = 255;
  c.g = 255;
  c.b = 255;
  c.a = 255;
  for (int i = 0; i < NUM_KEYS; i++)
  {
    result[i] = createtext(renderer, key_text[i], c);
  }
  return result;
}

void initplayers()
{
  for (int i = 0; i < 4; i++)
  {
    score[i] = 0;
    bombs[i] = 0;
    upgrade[i] = UPGRADE_NONE;
    upgradetime[i] = 0;
    keys[i] = 0;

    maxhealth[i] = (MAXHEALTH / 3) * char_healths[plytype[i]];

    if ((active_players & (1 << i)) > 0)
    {
      health[i] = maxhealth[i];
      loadplayersounds(i, plytype[i]);
      lowhealth[i] = false;
    }
    else
      health[i] = 0;
  }
}

bool setup(int argc, char * argv[])
{
  int i;
  int num_joysticks;

  loadconfig();

  use_sound = 1;

  for (i = 1; i < argc; i++)
  {
    if (0 == strcmp(argv[i], "--fullscreen"))
      fullscreen = 1;
    if (0 == strcmp(argv[i], "--windowed"))
      fullscreen = 0;
    if (0 == strcmp(argv[i], "--arcade"))
      arcade = true;
  }

  /* Initialize SDL */

  if (SDL_Init(SDL_INIT_VIDEO) < 0)
  {
    fprintf(stderr, "Error init'ing SDL: %s\n", SDL_GetError());
    exit(1);
  }

  if (SDL_Init(SDL_INIT_GAMECONTROLLER) < 0)
  {
    fprintf(stderr, "Warning: Error init'ing joystick: %s\n", SDL_GetError());
  }
  else
  {
    int maxJoysticks = SDL_NumJoysticks();
    int ply = 0;
    for(int JoystickIndex=0; JoystickIndex < maxJoysticks; ++JoystickIndex)
    {
      if (!SDL_IsGameController(JoystickIndex))
      {
        continue;
      }
      SDL_GameController *joystick = SDL_GameControllerOpen(JoystickIndex);
      controller_codes[ply].joy = SDL_JoystickInstanceID(SDL_GameControllerGetJoystick(joystick));
      ply++;
      if (ply == 4)
        break;
    }
  }

  if (use_sound)
  {
    if (SDL_Init(SDL_INIT_AUDIO) < 0)
    {
      fprintf(stderr, "Warning: Error init'ing audio: %s\n", SDL_GetError());
      use_sound = 0;
    }
  }

  if (use_sound)
  {
    if (Mix_OpenAudio(MIX_DEFAULT_FREQUENCY, MIX_DEFAULT_FORMAT, MIX_DEFAULT_CHANNELS, 4096) < 0)
    {
      fprintf(stderr, "Warning: Error opening audio: %s\n", SDL_GetError());
      use_sound = 0;
    }
  }

  if (TTF_Init() == -1)
  {
    fprintf(stderr, "Failed - TTF_Init\n");
    exit(1);
  }

  /* Open a window / screen */

  screen = SDL_CreateWindow("Fight or Perish", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREENW, SCREENH, (fullscreen ? SDL_WINDOW_FULLSCREEN_DESKTOP : 0));
  if (screen == NULL)
  {
    fprintf(stderr, "Error opening video: %s\n", SDL_GetError());
    SDL_Quit();
    exit(1);
  }
  renderer = SDL_CreateRenderer(screen, -1, 0);
  if (screen == NULL)
  {
    fprintf(stderr, "Error creating renderer: %s\n", SDL_GetError());
    SDL_Quit();
    exit(1);
  }
  SDL_RenderSetLogicalSize(renderer, SCREENW, SCREENH);

  /* Load imagery */

  loadimages(renderer);

  loadsounds();

  return arcade;
}

void updateplayer(int deltatime, int toggle, int scrollx, int scrolly, int i, controller_t *controller, arrow_t arrows[MAXARROWS], map_t *map)
{
  int got, gotx, wx, wy, wxx, wxy;
  int j, k, found;
  char c;

  if (controller->key_up)
    plydiry[i] = 1;
  else if (controller->key_down)
    plydiry[i] = 3;
  else if (controller->key_right || controller->key_left)
    plydiry[i] = 2;

  if (controller->key_left)
    plydirx[i] = 1;
  else if (controller->key_right)
    plydirx[i] = 3;
  else if (controller->key_up || controller->key_down)
    plydirx[i] = 2;

  if (health[i] > 0 && exited[i] == 0)
  {
    int oldplyx = plyx[i];
    int oldplyy = plyy[i];
    got = -1;

    if (controller->key_fire)
    {
      /* If pressing [Fire], and a direction, try to shoot: */

      found = -1;
      if (plycooldown[i] > 0)
      {
        plycooldown[i] -= 1 * deltatime;
        if (plycooldown[i] < 0)
          plycooldown[i] = 0;
      }
      if ((plycooldown[i] == 0) && ((controller->key_up) ||
          (controller->key_down) ||
          (controller->key_left) ||
          (controller->key_right)))
      {
        for (j = 0; j < MAXARROWS && found == -1; j++)
        {
          if (arrows[j].alive == 0)
            found = j;
        }
      }

      if (found != -1)
      {
        /* Add an arrow: */
        plycooldown[i] = PLAYER_COOLDOWN;

        arrows[found].alive = 1;
        arrows[found].x = plyx[i];
        arrows[found].y = plyy[i];

        if (controller->key_up)
          arrows[found].ym = -1 * move_speeds[3];
        else if (controller->key_down)
          arrows[found].ym = 1 * move_speeds[3];
        else
          arrows[found].ym = 0;

        if (controller->key_left)
          arrows[found].xm = -1 * move_speeds[3];
        else if (controller->key_right)
          arrows[found].xm = 1 * move_speeds[3];
        else
          arrows[found].xm = 0;

        /* Directional handicap for weak weapons: */
        if (char_weapons[plytype[i]] == 1 && upgrade[i] != UPGRADE_WEAPON)
        {
          if (arrows[found].xm != 0 && arrows[found].ym != 0)
          {
            if (toggle % 2)
              arrows[found].xm = 0;
            else
              arrows[found].xm = 0;
          }
        }

        if (plytype[i] == 2)
          Mix_PlayChannel(-1, snd_arrow, 0);
        else if (plytype[i] == 3)
          Mix_PlayChannel(-1, snd_fire, 0);
        else
          Mix_PlayChannel(-1, snd_spell, 0);
      }
    }
    else
    {
      /* Not pressing [Fire]; move the player, if they're pressing any arrow key(s): */

      if (controller->key_up && (plyy[i]/TILE_PRECISION) > scrolly - (SCREENH / 2))
      {
        int newy = sanitize(plyy[i] - deltatime * move_speeds[char_speeds[plytype[i]] + (upgrade[i] == UPGRADE_SPEED ? 1 : 0) - 1], move_speeds[char_speeds[plytype[i]] + (upgrade[i] == UPGRADE_SPEED ? 1 : 0) - 1]);
        got = safe(map, plyx[i], newy, TILEW * TILE_PRECISION - 1, TILEH * TILE_PRECISION - 1, toggle, &wx, &wy);
        if (got != -1)
        {
          plyy[i] = newy;
        }
      }
      else if (controller->key_down && (plyy[i]/TILE_PRECISION) + TILEH < scrolly + (SCREENH / 2))
      {
        int newy = sanitize(plyy[i] + deltatime * move_speeds[char_speeds[plytype[i]] + (upgrade[i] == UPGRADE_SPEED ? 1 : 0) - 1], move_speeds[char_speeds[plytype[i]] + (upgrade[i] == UPGRADE_SPEED ? 1 : 0) - 1]);
        got = safe(map, plyx[i], newy, TILEW * TILE_PRECISION - 1, TILEH * TILE_PRECISION - 1, toggle, &wx, &wy);
        if (got != -1)
        {
          plyy[i] = newy;
        }
      }

      if (controller->key_left && (plyx[i]/TILE_PRECISION) > scrollx - (SCREENW / 2))
      {
        int newx = sanitize(plyx[i] - deltatime * move_speeds[char_speeds[plytype[i]] + (upgrade[i] == UPGRADE_SPEED ? 1 : 0) - 1], move_speeds[char_speeds[plytype[i]] + (upgrade[i] == UPGRADE_SPEED ? 1 : 0) - 1]);
        gotx = safe(map, newx, plyy[i], TILEW * TILE_PRECISION - 1, TILEH * TILE_PRECISION - 1, toggle, &wxx, &wxy);
        if (gotx != -1)
        {
          plyx[i] = newx;
          got = gotx;
          wx = wxx;
          wy = wxy;
        }
      }
      else if (controller->key_right && (plyx[i]/TILE_PRECISION) + TILEW < scrollx + (SCREENW / 2) - 1)
      {
        int newx = sanitize(plyx[i] + deltatime * move_speeds[char_speeds[plytype[i]] + (upgrade[i] == UPGRADE_SPEED ? 1 : 0) - 1], move_speeds[char_speeds[plytype[i]] + (upgrade[i] == UPGRADE_SPEED ? 1 : 0) - 1]);
        gotx = safe(map, newx, plyy[i], TILEW * TILE_PRECISION - 1, TILEH * TILE_PRECISION - 1, toggle, &wxx, &wxy);
        if (gotx != -1)
        {
          plyx[i] = newx;
          got = gotx;
          wx = wxx;
          wy = wxy;
        }
      }
    }

    int who = collision(map, plyx[i], plyy[i], TILEW * TILE_PRECISION - 1, TILEH * TILE_PRECISION - 1, 0, NULL);
    if (who != -1)
    {
      if (plymelee[i] > 0)
      {
        plymelee[i] -= deltatime;
        if (plymelee[i] < 0)
          plymelee[i] = 0;
      }
      if (plymelee[i] == 0)
      {
        plymelee[i] = 4;
        switch (map->entities[who].type)
        {
          case ENTITY_ENEMY2:
          case ENTITY_ENEMY3:
          case ENTITY_EYE2:
          case ENTITY_EYE3:
          case ENTITY_GENERATOR2:
          case ENTITY_GENERATOR3:
          case ENTITY_EYEGEN2:
          case ENTITY_EYEGEN3:
            /* A bad guy or generator, higher than level 1:
               Reduce the bad guy or generator, bounce player back, reduce player's health */

            plyx[i] = oldplyx;
            plyy[i] = oldplyy;
            got = ' ';
            map->entities[who].type--;
            if (upgrade[i] != UPGRADE_HEALTH)
              health[i] -= 10;
            score[i] += 10;
            /* FIXME: Sound effect */
            break;
          case ENTITY_ENEMY1:
          case ENTITY_EYE1:
          case ENTITY_GENERATOR1:
          case ENTITY_EYEGEN1:
            /* A bad guy or generator, at level 1:
               Remove the bad guy or generator, reduce player's health */

            map->entities[who].type = ENTITY_NONE;
            if (upgrade[i] != UPGRADE_HEALTH)
              health[i] -= 10;
            score[i] += 10;
            break;
          case ENTITY_BLAST:
            map->entities[who].type = ENTITY_NONE;
            if (upgrade[i] != UPGRADE_HEALTH)
              health[i] -= 10;
            break;
          case ENTITY_GRIMREAPER:
            plyx[i] = oldplyx;
            plyy[i] = oldplyy;
            break;
          default:
            break;
        }
      }
      else
      {
        plyx[i] = oldplyx;
        plyy[i] = oldplyy;
        got = ' ';
      }
    }

    /* If they player bumped into something that we can act upon... */

    if (got == '$' || got == '&')
    {
      /* Money */

      setmap(map, wy, wx, ' ');
      score[i] += 10;
      Mix_PlayChannel(-1, snd_pickup, 0);
    }
    else if (got == '?')
    {
      /* Random bonus */

      setmap(map, wy, wx, ' ');
      upgrade[i] = rand() % NUM_UPGRADES;
      upgradetime[i] = UPGRADE_INIT_TIME;
      score[i] += 10;
      Mix_PlayChannel(-1, snd_upgrade, 0);
    }
    else if (got == 'b')
    {
      /* A bomb */

      setmap(map, wy, wx, ' ');
      bombs[i]++;
      Mix_PlayChannel(-1, snd_bombpickup, 0);
    }
    else if (got == 'k')
    {
      /* A key */

      setmap(map, wy, wx, ' ');
      keys[i]++;
      Mix_PlayChannel(-1, snd_key, 0);
    }
    else if (got == '=')
    {
      /* A door */

      if (keys[i] > 0)
      {
        /* We have a key; remove the door: */

        keys[i]--;
        opendoor(map, wx, wy);
        Mix_PlayChannel(-1, snd_open, 0);
      }
      else
      {
        /* No keys; user cannot go through the door! */

        plyx[i] = oldplyx;
        plyy[i] = oldplyy;
        /* FIXME: Sound effect */
      }
    }
    else if (got == '%')
    {
      if (plyx[i] >= (wx * TILEW - 10) * TILE_PRECISION &&
        plyx[i] <= (wx * TILEW + 10) * TILE_PRECISION &&
        plyy[i] >= (wy * TILEH - 10) * TILE_PRECISION &&
        plyy[i] <= (wy * TILEH + 10) * TILE_PRECISION)
      {
        exited[i] = 1;
        Mix_PlayChannel(-1, snd_transition, 0);
      }
    }
    else if (got == 'f')
    {
      /* Food */

      setmap(map, wy, wx, ' ');
      health[i] += (MAXHEALTH / 30);
      if (health[i] > maxhealth[i])
        health[i] = maxhealth[i];
      Mix_PlayChannel(-1, snd_eat, 0);
    }
    else if (got == 'w' || got == 'x' || got == 'y' || got == 'z')
    {
      /* A dead player's treasure */

      score[i] += 100;
      keys[i] += keys[got - 'w'];
      bombs[i] += bombs[got - 'w'];
      setmap(map, wy, wx, ' ');
      keys[got - 'w'] = 0;
      bombs[got - 'w'] = 0;
    }
  }


  /* Handle arrows: */
  for (j = 0; j < MAXARROWS; j++)
  {
    /* Since bad guys can move into the arrow's position as the arrow
       is about to go by, we check whether the arrow hit anything before and after
       moving it. */

    for (k = 0; k < 2; k++)
    {
      if (k == 1 && arrows[j].alive)
      {
        /* Arrow exists;
           If we've already checked for bad guys at the original position,
           move it (and see if it's still on the map) */

        arrows[j].x += arrows[j].xm * deltatime;
        arrows[j].y += arrows[j].ym * deltatime;
        if (arrows[j].y < 0 || arrows[j].y / TILE_PRECISION >= map->h * TILEH || arrows[j].x < 0 || arrows[j].x / TILE_PRECISION >= map->w * TILEW ||
            arrows[j].y / TILE_PRECISION < scrolly - (SCREENH / 2) ||
            arrows[j].y / TILE_PRECISION > scrolly + (SCREENH / 2) ||
            arrows[j].x / TILE_PRECISION < scrollx - (SCREENW / 2) ||
            arrows[j].x / TILE_PRECISION > scrollx + (SCREENW / 2))
        {
          arrows[j].alive = 0;
        }
      }

      if (arrows[j].alive)
      {
        /* Arrow still exists; see what it hit into */
        wy = (arrows[j].y + (TILEH + (arrows[j].ym < 0 ? -1 : 0)) * TILE_PRECISION / 2) / TILEH / TILE_PRECISION;
        wx = (arrows[j].x + (TILEW + (arrows[j].xm < 0 ? -1 : 0)) * TILE_PRECISION / 2) / TILEW / TILE_PRECISION;
        c = getmap(map, wy, wx);
        if (c == 'b')
        {
          /* A bomb; activate it now! */

          setmap(map, wy, wx, ' ');
          score[i] += (bomb(map, scrollx * TILE_PRECISION, scrolly * TILE_PRECISION) / 2);
          Mix_PlayChannel(-1, snd_bomb, 0);
        } 
        else if (c == 'f')
        {
          /* Food; remove it (don't shoot food!!!) */

          setmap(map, wy, wx, ' ');
          /* FIXME: Sound effect */
        } 


        /* Arrows go away once they hit _anything_ */
        if (c != ' ')
          arrows[j].alive = 0;

        int who = collision(map, arrows[j].x, arrows[j].y, TILEW * TILE_PRECISION - 1, TILEH * TILE_PRECISION - 1, 0, NULL);
        if (who != -1)
        {
          int points = entity_damage(&map->entities[who], DAMAGE_NORMAL);
          score[i] += points;
          if ((points > 0) || (map->entities[who].type == ENTITY_GRIMREAPER))
          {
              /* FIXME: Sound effect */

              /* Some players' arrows keep going after a kill */
              if ((map->entities[who].type != ENTITY_NONE) || (char_weapons[plytype[i]] + (upgrade[i] == UPGRADE_WEAPON ? 1 : 0) != 3))
                arrows[j].alive = 0;
          }
        }
      }
    }
  }
}

void updateentities(int deltatime, int toggle, int scrollx, int scrolly, map_t *map)
{
  int miny = (scrolly - (SCREENH / 2) - TILEH + 1) * TILE_PRECISION;
  int maxy = (scrolly + (SCREENH / 2)) * TILE_PRECISION;
  int minx = (scrollx - (SCREENW / 2) - TILEW + 1) * TILE_PRECISION;
  int maxx = (scrollx + (SCREENW / 2)) * TILE_PRECISION;
  int x2, y2, want;
  for (int i = 0; i < MAX_ENTITIES; i++)
  {
    if (map->entities[i].type == ENTITY_NONE)
      continue;
    if (map->entities[i].y < miny || map->entities[i].y > maxy ||
      map->entities[i].x < minx || map->entities[i].x > maxx)
      continue;
    if (map->entities[i].cooldown > 0)
    {
      map->entities[i].cooldown -= deltatime;
      if (map->entities[i].cooldown < 0)
        map->entities[i].cooldown = 0;
    }
    if (map->entities[i].type >= ENTITY_GENERATOR1 && map->entities[i].type <= ENTITY_EYEGEN3)
    {
      /* Generators */
      if (map->entities[i].cooldown == 0)
      {
        /* Pick a random adjacent spot */
        x2 = map->entities[i].x + ((rand() % 3) - 1) * TILEW * TILE_PRECISION;
        y2 = map->entities[i].y + ((rand() % 3) - 1) * TILEH * TILE_PRECISION;

        /* If the spot is empty, generate a bad guy of
           our level into the spot (but not one who
           can move this frame...) */

        if ((getmap(map, y2 / TILEH / TILE_PRECISION, x2 / TILEH / TILE_PRECISION) == ' ') && (-1 == collision(map, x2, y2, TILEW * TILE_PRECISION - 1, TILEH * TILE_PRECISION - 1, 0, NULL)))
        {
          createentity(map, map->entities[i].type - ENTITY_GENERATOR1 + ENTITY_ENEMY1, x2, y2, TILEW * TILE_PRECISION - 1, TILEH * TILE_PRECISION - 1, 0, 0, 0);
        }
        map->entities[i].cooldown = 20;
      }
    }
    else if ((map->entities[i].type >= ENTITY_ENEMY1 && map->entities[i].type <= ENTITY_ENEMY3) || (map->entities[i].type == ENTITY_GRIMREAPER))
    {
      want = closestplayer(map, plyx, plyy, health, map->entities[i].x, map->entities[i].y);
      monster_move(deltatime, toggle, map, i, want);
    }
    else if (map->entities[i].type >= ENTITY_EYE1 && map->entities[i].type <= ENTITY_EYE3)
    {
      if (map->entities[i].cooldown == 0)
      {
        want = closestplayer(map, plyx, plyy, health, map->entities[i].x, map->entities[i].y);
        int cx, cy;
        cx = map->entities[i].x;
        cy = map->entities[i].y;
        if (plyx[want] > cx + TILEW)
          cx = cx + TILEW;
        if (plyy[want] > cy + TILEW)
          cy = cy + TILEW;
        cx = abs(plyx[want] - cx) / TILE_PRECISION;
        cy = abs(plyy[want] - cy) / TILE_PRECISION;
        bool fire = false;
        int speedx, speedy;
        speedx = 0;
        speedy = 0;
        if (cx > TILEW)
        {
          if (cy <= 8)
          {
            fire = true;
            if (map->entities[i].x > plyx[want])
            {
              speedx = -blast_speed[0];
            }
            else
            {
              speedx = blast_speed[0];
            }
          }
          else if (cx >= cy - 8 && cx <= cy + 8)
          {
            fire = true;
            if (map->entities[i].x > plyx[want])
            {
              speedx = -blast_speed[0];
            }
            else
            {
              speedx = blast_speed[0];
            }
            if (map->entities[i].y > plyy[want])
            {
              speedy = -blast_speed[0];
            }
            else
            {
              speedy = blast_speed[0];
            }
          }
        }
        else if (cy > TILEW)
        {
          if (cx <= 8)
          {
            fire = true;
            if (map->entities[i].y > plyy[want])
            {
              speedy = -blast_speed[0];
            }
            else
            {
              speedy = blast_speed[0];
            }
          }
        }
        // Multiply by 2 in case creature in front of him moved first.
        y2 = map->entities[i].y + speedy * 2;
        x2 = map->entities[i].x + speedx * 2;
        int who = collision(map, x2, y2, TILEW * TILE_PRECISION - 1, TILEH * TILE_PRECISION - 1, 1, &i);
        if (who != -1)
        {
          fire = false;
        }

        if (fire)
        {
          createentity(map, ENTITY_BLAST, map->entities[i].x + (map->entities[i].w / 2 - BLASTW * TILE_PRECISION / 2) + 1, map->entities[i].y + (map->entities[i].h / 2 - BLASTH * TILE_PRECISION / 2) + 1, BLASTW * TILE_PRECISION - 1, BLASTH * TILE_PRECISION - 1, speedx, speedy, i);
          map->entities[i].cooldown = BLAST_COOLDOWN;
        }
        else
        {
          monster_move(deltatime, toggle, map, i, want);
        }
      }
    }
    else if (map->entities[i].type == ENTITY_BLAST)
    {
      SDL_Rect first, second;
      int wx, wy;
      char c;
      first.x = map->entities[i].x + map->entities[i].speedx * deltatime;
      first.y = map->entities[i].y + map->entities[i].speedy * deltatime;
      first.w = map->entities[i].w;
      first.h = map->entities[i].h;
      second.w = TILEW * TILE_PRECISION - 1;
      second.h = TILEH * TILE_PRECISION - 1;
      /* Arrow still exists; see what it hit into */
      wy = (map->entities[i].y + (BLASTH + (map->entities[i].speedy < 0 ? -1 : 0)) * TILE_PRECISION / 2) / TILEH / TILE_PRECISION;
      wx = (map->entities[i].x + (BLASTW + (map->entities[i].speedx < 0 ? -1 : 0)) * TILE_PRECISION / 2) / TILEW / TILE_PRECISION;
      c = getmap(map, wy, wx);
      if (c == 'b')
      {
        /* A bomb; activate it now! */

        setmap(map, wy, wx, ' ');
        score[i] += (bomb(map, scrollx * TILE_PRECISION, scrolly * TILE_PRECISION) / 2);
        Mix_PlayChannel(-1, snd_bomb, 0);
      }
      else if (c == 'f')
      {
        /* Food; remove it (don't shoot food!!!) */

        setmap(map, wy, wx, ' ');
        /* FIXME: Sound effect */
      }
      if (c == ' ')
      {
        for (int j = 0; j < 4; j++)
        {
          if (health[j] > 0 && exited[j] == 0)
          {
            second.x = plyx[j];
            second.y = plyy[j];
            if (check_collision(first, second))
            {
              if (upgrade[j] != UPGRADE_HEALTH)
                health[j] -= 10;
              map->entities[i].type = ENTITY_NONE;
            }
          }
        }
        int skip[2];
        skip[0] = i;
        skip[1] = map->entities[i].data;
        int who = collision(map, first.x, first.y, first.w, first.h, 2, skip);
        if (who != -1)
        {
          int points = entity_damage(&map->entities[who], DAMAGE_NORMAL);
          if (points > 0)
            map->entities[i].type = ENTITY_NONE;
        }
      }
      else
        map->entities[i].type = ENTITY_NONE;
      if (map->entities[i].type != ENTITY_NONE)
      {
        map->entities[i].x = first.x;
        map->entities[i].y = first.y;
      }
    }
  }
}

int entity_damage(entity_t *entity, int damagetype)
{
  switch(entity->type)
  {
    case ENTITY_ENEMY2:
    case ENTITY_ENEMY3:
    case ENTITY_EYE2:
    case ENTITY_EYE3:
    case ENTITY_GENERATOR2:
    case ENTITY_GENERATOR3:
    case ENTITY_EYEGEN2:
    case ENTITY_EYEGEN3:
      /* A bad guy or generator, higher than level 1:
         Reduce the bad guy or generator */

      entity->type--;
      return 10;
      break;
    case ENTITY_ENEMY1:
    case ENTITY_EYE1:
    case ENTITY_GENERATOR1:
    case ENTITY_EYEGEN1:
      /* A bad guy or generator, at level 1:
         Remove the bad guy or generator */

      entity->type = ENTITY_NONE;
      return 10;
      break;
    case ENTITY_GRIMREAPER:
      if (damagetype == DAMAGE_BOMB)
      {
        entity->type = ENTITY_NONE;
        return 10;
      }
      break;
    default:
      break;
  }
  return 0;
}

void monster_move(int deltatime, int toggle, map_t * map, int i, int want)
{
  /* For those objects interested, which way do we go to
     head towards the player */
  int x2, y2;

  if ((plyy[want] > map->entities[i].y) && (plyy[want] >= map->entities[i].y + entity_speed[0]))
    y2 = map->entities[i].y + deltatime * entity_speed[0];
  else if ((plyy[want] < map->entities[i].y) && (plyy[want] <= map->entities[i].y - entity_speed[0]))
    y2 = map->entities[i].y - deltatime * entity_speed[0];
  else
    y2 = map->entities[i].y;

  if ((plyx[want] > map->entities[i].x) && (plyx[want] >= map->entities[i].x + entity_speed[0]))
    x2 = map->entities[i].x + deltatime * entity_speed[0];
  else if ((plyx[want] < map->entities[i].x) && (plyx[want] <= map->entities[i].x - entity_speed[0]))
    x2 = map->entities[i].x - deltatime * entity_speed[0];
  else
    x2 = map->entities[i].x;

  if (toggle % 8 == 0)
    map->entities[i].anim = rand() % 2;
  if (!monster_trymove(map, i, x2, y2))
  {
    if (!monster_trymove(map, i, map->entities[i].x, y2))
    {
      monster_trymove(map, i, x2, map->entities[i].y);
    }
  }
}

int monster_trymove(map_t *map, int entitynum, int x, int y)
{
  int wx, wy;
  int c = safe(map, x, y, TILEW * TILE_PRECISION - 1, TILEH * TILE_PRECISION - 1, 0, &wx, &wy);
  if (c == ' ')
  {
    SDL_Rect first, second;
    first.x = x;
    first.y = y;
    first.w = map->entities[entitynum].w;
    first.h = map->entities[entitynum].h;
    second.w = TILEW * TILE_PRECISION - 1;
    second.h = TILEH * TILE_PRECISION - 1;
    for (int i = 0; i < 4; i++)
    {
      if (health[i] > 0 && exited[i] == 0)
      {
        second.x = plyx[i];
        second.y = plyy[i];
        if (check_collision(first, second))
        {
          if (map->entities[entitynum].cooldown == 0)
          {
            /* If player is not trying to attack us,
               try to attack them.
               (Our chance of succeeding in the melee attack depends
               on what level we are) */

            if (map->entities[entitynum].type == ENTITY_GRIMREAPER)
            {
              health[i] -= 2;
              map->entities[entitynum].data += 2;
              if (map->entities[entitynum].data > (MAXHEALTH / 4))
              {
                map->entities[entitynum].type = ENTITY_NONE;
              }
            }
            else
            {
              map->entities[entitynum].cooldown = MELEE_COOLDOWN;
              if (upgrade[i] != UPGRADE_HEALTH)
              {
                if (map->entities[entitynum].type == ENTITY_ENEMY1 && (rand() % 10) < 3)
                  health[i] -= 10;
                else if (map->entities[entitynum].type == ENTITY_ENEMY2 && (rand() % 10) < 5)
                  health[i] -= 10;
                else if (map->entities[entitynum].type == ENTITY_ENEMY3 && (rand() % 10) < 7)
                  health[i] -= 10;
                else if (map->entities[entitynum].type == ENTITY_EYE1 && (rand() % 10) < 3)
                  health[i] -= 10;
                else if (map->entities[entitynum].type == ENTITY_EYE2 && (rand() % 10) < 5)
                  health[i] -= 10;
                else if (map->entities[entitynum].type == ENTITY_EYE3 && (rand() % 10) < 7)
                  health[i] -= 10;

                if (health[i] < oldhealth[i])
                {
                  /* FIXME: Sound effect */
                }
              }
            }
          }
          return 1;
        }
      }
    }
    int who = collision(map, x, y, TILEW * TILE_PRECISION - 1, TILEH * TILE_PRECISION - 1, 1, &entitynum);
    if (who != -1)
    {
      return 0;
    }
    map->entities[entitynum].x = x;
    map->entities[entitynum].y = y;
    return 1;
  }
  return 0;
}

void drawplayer(int toggle, int scrollx, int scrolly, controller_t *controller, int i, arrow_t arrows[4])
{
  SDL_Rect dest;
  dest.x = plyx[i] / TILE_PRECISION - scrollx + SCREENW / 2;
  dest.y = plyy[i] / TILE_PRECISION - scrolly + SCREENH / 2;
  dest.w = TILEW;
  dest.h = TILEH;
  if (health[i] > 0 && exited[i] == 0)
  {
    /* Flash around player if they just got hurt */

    if (hurting[i])
    {
      SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
      SDL_RenderFillRect(renderer, &dest);
      SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
    }

    drawimage(renderer, img_player[plytype[i]], 6, 12,
      plydirx[i] + (((controller->key_up || controller->key_down || controller->key_left || controller->key_right) && !controller->key_fire) ? (3 * ((toggle & 4) >> 2)) : 0),
      plydiry[i] + (i * 3),
      dest.x, dest.y);
  }

  /* Draw any arrows we can still see: */

  for (int j = 0; j < MAXARROWS; j++)
  {
    if (arrows[j].alive)
    {
      dest.x = arrows[j].x / TILE_PRECISION - scrollx + SCREENW / 2;
      dest.y = arrows[j].y / TILE_PRECISION - scrolly + SCREENH / 2;
      int xm = (arrows[j].xm > 0) ? 1 : ((arrows[j].xm < 0) ? -1 : 0);
      int ym = (arrows[j].ym > 0) ? 1 : ((arrows[j].ym < 0) ? -1 : 0);
      drawimage(renderer, img_arrows[plytype[i]], 3, 3, 2 + xm, 2 + ym, dest.x, dest.y);
    }
  }
}

void drawtiles(map_t *map, int scrollx, int scrolly)
{
  SDL_Rect dest;
  char c;
  for (int y = (scrolly - (SCREENH / 2)) / TILEH; y <= (scrolly + (SCREENH / 2)) / TILEH + 1; y++)
  {
    for (int x = (scrollx - (SCREENW / 2)) / TILEW; x <= (scrollx + (SCREENW / 2)) / TILEW; x++)
    {
      if (y >= 0 && y < map->h && x >= 0 && x < map->w)
      {
        /* Draw map objects: */

        c = getmap(map, y, x);
        dest.x = (x * TILEW) - scrollx + SCREENW / 2;
        dest.y = (y * TILEH) - scrolly + SCREENH / 2;
        dest.w = TILEW;
        dest.h = TILEH;

        unsigned char floor = getfloor(map, y, x);
        unsigned char wall = getwall(map, y, x);
        if (floor > 0)
        {
          drawtile(renderer, img_tiles, 32, 32, 2, floor - 1, dest.x, dest.y);
          if (wall > 0)
            drawtile(renderer, img_tiles, 32, 32, 2, wall - 1, dest.x, dest.y);
        }
        if ((floor != 0) && (c != '#'))
        {
          int wallleft = 0;
          int wallcorner = 0;
          int walldown = 0;
          if ((x > 0) && (getmap(map, y, x - 1) == '#'))
          {
            wallleft = 1;
          }
          if ((x > 0) && (y < map->h) && (getmap(map, y + 1, x - 1) == '#'))
          {
            wallcorner = 1;
          }
          if ((y < map->h) && (getmap(map, y + 1, x) == '#'))
          {
            walldown = 1;
          }
          if (wallleft && wallcorner && walldown)
            drawtile(renderer, img_tiles, 32, 32, 2, 27 + 32, dest.x, dest.y);
          else if (wallleft && walldown)
            drawtile(renderer, img_tiles, 32, 32, 2, 25 + 32, dest.x, dest.y);
          else if (wallleft && wallcorner)
            drawtile(renderer, img_tiles, 32, 32, 2, 26 + 32, dest.x, dest.y);
          else if (wallcorner && walldown)
            drawtile(renderer, img_tiles, 32, 32, 2, 24 + 32, dest.x, dest.y);
          else if (wallleft)
            drawtile(renderer, img_tiles, 32, 32, 2, 11 + 32, dest.x, dest.y);
          else if (wallcorner)
            drawtile(renderer, img_tiles, 32, 32, 2, 10 + 32, dest.x, dest.y);
          else if (walldown)
            drawtile(renderer, img_tiles, 32, 32, 2, 9 + 32, dest.x, dest.y);
        }
        if (c == '#')
        {
        }
        else if (c == '=')
        {
          drawtile(renderer, img_tiles, 32, 32, 2, wall + 7, dest.x, dest.y);
        }
        else if (c == '$')
          drawimage(renderer, img_collectibles, 6, 1, 4, 1, dest.x, dest.y);
        else if (c == '&')
          drawimage(renderer, img_collectibles, 6, 1, 5, 1, dest.x, dest.y);
        else if (c == '?')
          drawimage(renderer, img_collectibles, 6, 1, 6, 1, dest.x, dest.y);
        else if (c == '%')
          drawtile(renderer, img_tiles, 32, 32, 2, 128, dest.x, dest.y);
        else if (c == 'f')
          drawimage(renderer, img_collectibles, 6, 1, 1, 1, dest.x, dest.y);
        else if (c == 'k')
          drawimage(renderer, img_collectibles, 6, 1, 2, 1, dest.x, dest.y);
        else if (c == 'b')
          drawimage(renderer, img_collectibles, 6, 1, 3, 1, dest.x, dest.y);
        else if (c == '!')
          drawimage(renderer, img_grimreaper, 6, 3, 2, 3, dest.x, dest.y);
        else if (c >= '1' && c <= '3')
          drawimage(renderer, img_generators[0], 3, 1, c - '1' + 1, 1, dest.x, dest.y);
        else if (c >= 'M' && c <= 'O')
          drawimage(renderer, img_generators[1], 3, 1, c - 'M' + 1, 1, dest.x, dest.y);
        else if (c >= 'A' && c <= 'F')
          drawimage(renderer, img_badguys[(c - 'A') / 3], 6, 9, 2, ((c - 'A') % 3) * 3 + 3, dest.x, dest.y);
      }
    }
  }
}

void drawentities(map_t *map, int scrollx, int scrolly)
{
  int miny = (scrolly - (SCREENH / 2)) - TILEH * TILE_PRECISION;
  int maxy = (scrolly + (SCREENH / 2)) * TILE_PRECISION;
  int minx = (scrollx - (SCREENW / 2)) - TILEW * TILE_PRECISION;
  int maxx = (scrollx + (SCREENW / 2)) * TILE_PRECISION;
  /* Draw monsters */
  for (int i = 0; i < MAX_ENTITIES; i++)
  {
    if (map->entities[i].type == ENTITY_NONE)
      continue;
    if (minx > map->entities[i].x || maxx < map->entities[i].x ||
      miny > map->entities[i].y || maxy < map->entities[i].y)
    {
      continue;
    }
    if (map->entities[i].type == ENTITY_GRIMREAPER)
    {
      int x = map->entities[i].x / TILE_PRECISION - scrollx + SCREENW / 2;
      int y = map->entities[i].y / TILE_PRECISION - scrolly + SCREENH / 2;
      int want = closestplayer(map, plyx, plyy, health, map->entities[i].x, map->entities[i].y);
      drawimage(renderer, img_grimreaper, 6, 3,
        (plyx[want] < map->entities[i].x ? 1 : (plyx[want] == map->entities[i].x ? 2 : 3)) + (3 * map->entities[i].anim),
        (plyy[want] < map->entities[i].y ? 1 : (plyy[want] == map->entities[i].y ? 2 : 3)) + (3 * ((map->entities[i].type - ENTITY_ENEMY1) % 3)),
        x, y);
    }
    if ((map->entities[i].type >= ENTITY_ENEMY1) && (map->entities[i].type <= ENTITY_EYE3))
    {
      int badguy = (map->entities[i].type - ENTITY_ENEMY1) / 3;
      int x = map->entities[i].x / TILE_PRECISION - scrollx + SCREENW / 2;
      int y = map->entities[i].y / TILE_PRECISION - scrolly + SCREENH / 2;
      int want = closestplayer(map, plyx, plyy, health, map->entities[i].x, map->entities[i].y);
      drawimage(renderer, img_badguys[badguy], 6, 9,
        (plyx[want] < map->entities[i].x ? 1 : (plyx[want] == map->entities[i].x ? 2 : 3)) + (3 * map->entities[i].anim),
        (plyy[want] < map->entities[i].y ? 1 : (plyy[want] == map->entities[i].y ? 2 : 3)) + (3 * ((map->entities[i].type - ENTITY_ENEMY1) % 3)),
        x, y);
    }
    if ((map->entities[i].type >= ENTITY_GENERATOR1) && (map->entities[i].type <= ENTITY_EYEGEN3))
    {
      int generator = (map->entities[i].type - ENTITY_GENERATOR1) / 3;
      int x = map->entities[i].x / TILE_PRECISION - scrollx + SCREENW / 2;
      int y = map->entities[i].y / TILE_PRECISION - scrolly + SCREENH / 2;
      drawimage(renderer, img_generators[generator], 3, 1, 1 + ((map->entities[i].type - ENTITY_GENERATOR1) % 3), 1, x, y);
    }
    if (map->entities[i].type == ENTITY_BLAST)
    {
      int x = map->entities[i].x / TILE_PRECISION - scrollx + SCREENW / 2;
      int y = map->entities[i].y / TILE_PRECISION - scrolly + SCREENH / 2;
      drawimage(renderer, img_blast, 4, 1, 1, 1, x, y);
    }
  }
}

void drawhud(int toggle)
{
  SDL_Rect dest;
  int x, y;
  int i, j;
  char str[16];
  for (i = 0; i < 4; i++)
  {
    /* FIXME: Better HUD display */

    if (health[i] > 0)
    {
      /* Draw our health meter */

      y = ((TILEH * 4) * i) + TILEH / 4;

      drawimage(renderer, img_stats, 4, 1, 1, 1, 0, y);

      /* Max health */

      dest.x = img_stats->w / 4;
      dest.y = y;
      dest.w = (maxhealth[i] * (TILEW * 4)) / MAXHEALTH;
      dest.h = img_stats->h;

      SDL_SetRenderDrawColor(renderer,
        ((char_colors[i] & 0xff0000) >> 16) / 2,
        ((char_colors[i] & 0x00ff00) >> 8) / 2,
        ((char_colors[i] & 0x0000ff) / 2), 255);
      SDL_RenderFillRect(renderer, &dest);

      /* Current meter: */

      dest.x = img_stats->w / 4;
      dest.y = y;
      dest.w = (health[i] * (TILEW * 4)) / MAXHEALTH;
      dest.h = img_stats->h;

      /* Flash if it's low! */

      if (health[i] > maxhealth[i] / 4 || (toggle % 2) == 0)
      {
        SDL_SetRenderDrawColor(renderer,
          (char_colors[i] & 0xff0000) >> 16,
          (char_colors[i] & 0x00ff00) >> 8,
          (char_colors[i] & 0x0000ff), 255);
        SDL_RenderFillRect(renderer, &dest);
      }
      else
      {
        SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255);
        SDL_RenderFillRect(renderer, &dest);
      }


      /* Draw our collected objects */


      /* Treasure */

      dest.x = img_stats->w / 4;
      dest.y = y + img_stats->h;

      drawimage(renderer, img_stats, 4, 1, 4, 1, 0, dest.y);
      
      snprintf(str, sizeof(str), "%06d", score[i]);
      for (j = 0; j < strlen(str); j++)
      {
        x = (img_stats->w / 4) + (img_numbers->w / 10) * j;
        drawimage(renderer, img_numbers, 10, 1, (str[j] - '0') + 1, 1, x, y + img_stats->h);
      }

      /* Keys */

      for (j = 0; j < keys[i]; j++)
      {
        dest.x = (j * TILEW);
        dest.y = y + img_stats->h * 2;
        drawimage(renderer, img_collectibles, 6, 1, 2, 1, dest.x, dest.y);
      }

      /* Bombs */

      for (j = 0; j < bombs[i]; j++)
      {
        dest.x = (j * TILEW);
        dest.y = y + img_stats->h * 2 + TILEH;
        drawimage(renderer, img_collectibles, 6, 1, 3, 1, dest.x, dest.y);
      }

      /* Any temporary upgrade */
      if (upgrade[i] != UPGRADE_NONE)
      {
        dest.x = 0;
        dest.y = y + img_stats->h * 2 + TILEH * 2;
        drawimage(renderer, img_stats, 4, 1, upgrade[i] + 1, 1, dest.x, dest.y);

        dest.x = img_stats->w / 4;
        dest.w = (upgradetime[i] * (TILEW * 4)) / UPGRADE_INIT_TIME;
        dest.h = img_stats->h;
        SDL_SetRenderDrawColor(renderer, 0xff, 0xff, 0xff, 255);
        SDL_RenderFillRect(renderer, &dest);
      }
      SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
    }
  }
}

void drawtitle(int scrollx, int scrolly)
{
  SDL_Rect dest;
  if (demostate.pos == 0)
  {
    dest.x = (SCREENW - img_bigtitle->w) / 2;
    dest.y = (SCREENH - img_bigtitle->h) / 2;
    dest.w = img_bigtitle->w;
    dest.h = img_bigtitle->h;
    SDL_RenderCopy(renderer, img_bigtitle->tex, NULL, &dest);
  }
  else
  {
    dest.x = (SCREENW - img_title->w) / 2;
    dest.y = 10;
    dest.w = img_title->w;
    dest.h = img_title->h;
    SDL_RenderCopy(renderer, img_title->tex, NULL, &dest);
  }
  if (demostate.instruction != 0)
  {
    dest.x = demostate.x * TILEW -(scrollx - SCREENW / 2);
    dest.y = demostate.y * TILEH -(scrolly - SCREENH / 2);
    dest.w = img_instructions[demostate.instruction - 1]->w;
    dest.h = img_instructions[demostate.instruction - 1]->h;
    SDL_RenderCopy(renderer, img_instructions[demostate.instruction - 1]->tex, NULL, &dest);
  }
}
