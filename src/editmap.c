#include <SDL.h>
#include "controller.h"
#include "game.h"
#include "resource.h"
#include "editmap.h"
#include "levelselect.h"
#include "menu.h"
#include "selectlist.h"
#include "scene.h"

#define NUM_TILES 20
static char tiletype[NUM_TILES] = {
  '#',
  '=',
  '%',
  '$',
  'f',
  'b',
  'k',
  '1',
  '2',
  '3',
  'M',
  'N',
  'O',
  '!',
  'A',
  'B',
  'C',
  'D',
  'E',
  'F'
};
static const char *tilename[NUM_TILES] = {
  "Wall",
  "Door",
  "Exit",
  "Gold",
  "Food",
  "Bomb",
  "Key",
  "Skeleton Generator 1",
  "Skeleton Generator 2",
  "Skeleton Generator 3",
  "Eye Terror Generator 1",
  "Eye Terror Generator 2",
  "Eye Terror Generator 3",
  "Grim Reaper",
  "Skeleton 1",
  "Skeleton 2",
  "Skeleton 3",
  "Eye Terror 1",
  "Eye Terror 2",
  "Eye Terror 3"
};

static editmapdata_t *editdata = NULL;

void *editmapinit()
{
  editmapdata_t *data = (editmapdata_t*)malloc(sizeof(editmapdata_t));
  memset(data, 0, sizeof(editmapdata_t));
  for (int i = 0; i < 4; i++)
  {
    for (int j = 0; j < 4; j++)
    {
      data->prevdir[i][j] = 0;
    }
  }
  data->map = loadmap(2);
  data->tiletype = '#';
  data->enter = true;
  editdata = data;
  return data;
}

void editmapfree(void *data)
{
  editmapdata_t *edata = (editmapdata_t*)data;
  freemap(&edata->map);
  free(edata);
}

bool editmapupdate(int deltatime, bool top, void *data)
{
  int oldkey_fire[4];
  int i;
  editmapdata_t *edata = (editmapdata_t*)data;
  if (!top)
    return true;

  edata->toggle++;
  scrollx = edata->whx * TILEW;
  scrolly = edata->why * TILEH;
  if (scrollx < SCREENW / 2)
    scrollx = SCREENW / 2;
  else if (edata->map->w * TILEW <= scrollx + SCREENW / 2)
    scrollx = edata->map->w * TILEW - SCREENW / 2;
  if (scrolly < SCREENH / 2)
    scrolly = SCREENH / 2;
  else if (edata->map->h * TILEH <= scrolly + SCREENH / 2)
    scrolly = edata->map->h * TILEH - SCREENH / 2;

  for (i = 0; i < 4; i++)
    oldkey_fire[i] = controllers[i].key_fire;
  int result = processevent(controllers, NULL);
  if (edata->enter)
  {
    bool done = true;
    for (i = 0; i < 4; i++)
    {
      if (1 == controllers[i].key_fire)
        done = false;
    }
    if (done)
      edata->enter = false;
  }
  if (result == PROCESS_QUIT)
    return false;
  if (result == PROCESS_BACK)
  {
    replacescene(NULL, NULL, NULL, NULL);
    return true;
  }
  for (i = 0; i < 4; i++)
  {
    if ((controllers[i].key_fire) && (!edata->enter))
    {
      setmap(edata->map, edata->why, edata->whx, edata->tiletype);
      processwalls(edata->map);
    }
    if (controllers[i].key_bomb)
    {
      setmap(edata->map, edata->why, edata->whx, ' ');
      processwalls(edata->map);
    }
    if (controllers[i].key_start)
    {
      menudata_t *menudata = menuinit();
      menuadd(menudata, "Change Tile", selectupdate, selectdraw, selectfree, tiletypeselectinit, SCENE_REPLACELAST);
      menuadd(menudata, "Load", levelselectupdate, levelselectdraw, levelselectfree, levelloadinit, SCENE_REPLACELAST);
      menuadd(menudata, "Save", levelselectupdate, levelselectdraw, levelselectfree, levelsaveinit, SCENE_REPLACELAST);
      menuadd(menudata, "Exit", NULL, NULL, NULL, NULL, SCENE_REPLACELAST);
      addscene(menuupdate, menudraw, menufree, menudata, false);
    }
    if (edata->prevdir[i][0] > 0)
    {
      edata->prevdir[i][0]--;
    }
    else if (controllers[i].key_up)
    {
      edata->prevdir[i][0] = 3;
      edata->why--;
      if (edata->why < 0)
        edata->why = 0;
    }
    if (edata->prevdir[i][1] > 0)
    {
      edata->prevdir[i][1]--;
    }
    else if (controllers[i].key_down)
    {
      edata->prevdir[i][1] = 3;
      edata->why++;
      if (edata->why >= edata->map->h)
        edata->why = edata->map->h - 1;
    }
    if (edata->prevdir[i][2] > 0)
    {
      edata->prevdir[i][2]--;
    }
    else if (controllers[i].key_left)
    {
      edata->prevdir[i][2] = 3;
      edata->whx--;
      if (edata->whx < 0)
        edata->whx = 0;
    }
    if (edata->prevdir[i][3] > 0)
    {
      edata->prevdir[i][3]--;
    }
    else if (controllers[i].key_right)
    {
      edata->prevdir[i][3] = 3;
      edata->whx++;
      if (edata->whx >= edata->map->w)
        edata->whx = edata->map->w - 1;
    }
  }
  return true;
}

void editmapdraw(void *data)
{
  editmapdata_t *edata = (editmapdata_t*)data;

  SDL_RenderClear(renderer);
  drawtiles(edata->map, scrollx, scrolly);
  {
    int x = edata->whx * TILEW - scrollx + SCREENW/2;
    int y = edata->why * TILEH - scrolly + SCREENH/2;
    SDL_Rect src, dest;

    src.x = (TILEW + 2) * (14 + ((edata->toggle / 4) % 2));
    src.y = (TILEH + 2) * (8);
    src.w = TILEW;
    src.h = TILEH;

    dest.x = x;
    dest.y = y;
    dest.w = TILEW;
    dest.h = TILEH;

    SDL_RenderCopy(renderer, img_tiles->tex, &src, &dest);
  }
}

void levelsave(int level)
{
  writemap(level, editdata->map);
  editdata->enter = true;
}

void levelload(int level)
{
  editdata->map = loadmap(level);
  editdata->whx = 0;
  editdata->why = 0;
  editdata->enter = true;
}

void *levelsaveinit()
{
  return levelselectinit(levelsave);
}

void *levelloadinit()
{
  return levelselectinit(levelload);
}

void tiletypeset(int choice)
{
  editdata->tiletype = tiletype[choice];
  editdata->enter = true;
}

void *tiletypeselectinit()
{
  selectdata_t *data = selectinit();
  for (int i = 0; i < NUM_TILES; i++)
  {
    selectadd(data, tilename[i], tiletypeset);
  }
  return data;
}
