#ifndef __GAME_H
#define __GAME_H
/*
  FOP: Fight or Perish

  Based on Jack Pavelich's "Dandy" ('Thesis of Terror')
  and Atari Games' "Gauntlet"

  by Bill Kendrick <bill@newbreedsoftware.com>
  http://www.newbreedsoftware.com/fop/

  February 25, 2009 - March 15, 2009
*/

#include <stdbool.h>
#include "resource.h"

#define SCREENW 640
#define SCREENH 480

#define FPS 40

#define NUM_MENU 5

#define MAXARROWS 3

typedef struct arrow_s {
  int alive;
  int x, y, xm, ym;
} arrow_t;

typedef struct gamedata_s gamedata_t;

typedef int (*processinputfunc)(controller_t controllers[4], map_t *map);
typedef gamedata_t *(*nextlevelfunc)(gamedata_t *gdata);

typedef struct gamedata_s {
  int level;
  map_t * map;
  processinputfunc processinput;
  nextlevelfunc nextlevel;
  arrow_t arrows[4][MAXARROWS];
  int toggle;
} gamedata_t;

extern int char_speeds[4];
extern int char_healths[4];
extern int char_weapons[4];

extern SDL_Window * screen;
extern SDL_Renderer *renderer;

extern int use_sound;

extern bool welcome;
extern int active_players;
extern int plytypebak;
extern int plytype[4];
extern int level;
extern int scrollx, scrolly;
extern bool arcade;

#define sign(x) (((x) < 0) ? -1 : (((x) > 0) ? 1 : 0))
extern gamedata_t *demonextlevel(gamedata_t *gdata);
extern gamedata_t *gamenextlevel(gamedata_t *gdata);
extern gamedata_t *gameinit(int level, processinputfunc processinput, nextlevelfunc nextlevel);
extern void gamefree(void *data);
extern bool gameupdate(int deltatime, bool top, void *data);
extern void gamedraw(void *data);
extern bool setup(int argc, char * argv[]);
extern int safe(map_t * map, int x, int y, int w, int h, int toggle, int *wx, int *wy);
extern int bomb(map_t * map, int plyx, int plyy);
extern void opendoor(map_t * map, int x, int y);
extern void drawimage(SDL_Renderer * renderer, texture_t * sheet, int tilew, int tileh, int tilex, int tiley, int x, int y);
extern void drawtile(SDL_Renderer * renderer, texture_t * sheet, int tilesw, int tilesh, int spacer, int tile, int x, int y);
extern int closestplayer(map_t * map, int plyx[4], int plyy[4], int health[4], int x, int y);
extern texture_t **loadkeytext();
extern void initplayers();
extern void updateplayer(int deltatime, int toggle, int scrollx, int scrolly, int i, controller_t *controller, arrow_t arrows[MAXARROWS], map_t *map);
extern void updateentities(int deltatime, int toggle, int scrollx, int scrolly, map_t *map);
extern int entity_damage(entity_t *entity, int damagetype);
extern void monster_move(int deltatime, int toggle, map_t * map, int i, int want);
extern int monster_trymove(map_t * map, int entitynum, int x, int y);
extern void drawplayer(int toggle, int scrollx, int scrolly, controller_t *controller, int i, arrow_t arrows[4]);
extern void drawtiles(map_t *map, int scrollx, int scrolly);
extern void drawentities(map_t *map, int scrollx, int scrolly);
extern void drawhud(int toggle);
extern void drawtitle(int scrollx, int scrolly);extern bool setup(int argc, char * argv[]);

#endif
