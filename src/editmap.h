#ifndef __EDITMAP_H
#define __EDITMAP_H

typedef struct editmapdata_s {
  int prevdir[4][4];
  int whx, why;
  map_t *map;
  int toggle;
  char tiletype;
  bool enter;
} editmapdata_t;

void *editmapinit();
void editmapfree(void *data);
bool editmapupdate(int deltatime, bool top, void *data);
void editmapdraw(void *data);

void *levelsaveinit();
void *levelloadinit();

void *tiletypeselectinit();

#endif
