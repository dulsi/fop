#ifndef __OPTION_H
#define __OPTION_H

typedef struct optiondata_s {
  texture_t *img_options[11];
  int prevfire[4];
  int prevup[4];
  int prevdown[4];
  int option;
} optiondata_t;

void *optioninit();
void optionfree(void *data);
bool optionupdate(int deltatime, bool top, void *data);
void optiondraw(void *data);

#endif
