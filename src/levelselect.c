#include <SDL.h>
#include "controller.h"
#include "game.h"
#include "resource.h"
#include "levelselect.h"
#include "scene.h"

void *levelselectinit(setlevelfunc sl)
{
  levelselectdata_t *data = (levelselectdata_t*)malloc(sizeof(levelselectdata_t));
  memset(data, 0, sizeof(levelselectdata_t));
  data->setlevel = sl;
  for (int i = 0; i < 4; i++)
  {
    data->prevfire[i] = 1;
    for (int j = 0; j < 2; j++)
    {
      data->prevdir[i][j] = 0;
    }
  }
  data->number = 1;
  return data;
}

void levelselectfree(void *data)
{
  levelselectdata_t *ldata = (levelselectdata_t*)data;
  free(ldata);
}

bool levelselectupdate(int deltatime, bool top, void *data)
{
  int i;
  levelselectdata_t *ldata = (levelselectdata_t*)data;

  int result = processevent(controllers, NULL);
  if (result == PROCESS_QUIT)
    return false;
  if (result == PROCESS_BACK)
  {
    replacelast(NULL, NULL, NULL, NULL);
    return true;
  }
  for (i = 0; i < 4; i++)
  {
    if (ldata->prevdir[i][0] > 0)
    {
      ldata->prevdir[i][0]--;
    }
    else if (controllers[i].key_down)
    {
      ldata->prevdir[i][0] = 3;
      ldata->number--;
      if (ldata->number < 1)
        ldata->number = 99;
    }
    if (ldata->prevdir[i][1] > 0)
    {
      ldata->prevdir[i][1]--;
    }
    else if (controllers[i].key_up)
    {
      ldata->prevdir[i][1] = 3;
      ldata->number++;
      if (ldata->number >= 100)
        ldata->number = 1;
    }
    if ((controllers[i].key_fire) && (0 == ldata->prevfire[i]))
    {
      (ldata->setlevel)(ldata->number);
      replacelast(NULL, NULL, NULL, NULL);
    }
    ldata->prevfire[i] = controllers[i].key_fire;
  }
  return true;
}

void levelselectdraw(void *data)
{
  SDL_Rect src, dest;
  int i, j;
  char str[16];
  levelselectdata_t *ldata = (levelselectdata_t*)data;

  int width = TILEW * 2;
  int height = TILEH * 2;

  for (i = 0; i < width; i += TILEW)
  {
    for (j = 0; j < height; j += TILEH)
    {
      if (i == 0 && j == 0)
        src.x = (TILEW + 2) * 5;
      else if (i == width - TILEW && j == 0)
        src.x = (TILEW + 2) * 7;
      else if (j == 0)
        src.x = (TILEW + 2) * 6;
      else if (i == 0 && j == height - TILEH)
        src.x = (TILEW + 2) * 11;
      else if (i == width - TILEW && j == height - TILEH)
        src.x = (TILEW + 2) * 13;
      else if (i == 0)
        src.x = (TILEW + 2) * 8;
      else if (i == width - TILEW)
        src.x = (TILEW + 2) * 10;
      else if (j == height - TILEH)
        src.x = (TILEW + 2) * 12;
      else
        src.x = (TILEW + 2) * 9;
      src.y = (TILEH + 2) * (8);
      src.w = TILEW;
      src.h = TILEH;
      dest.x = (SCREENW - width) / 2 + i;
      dest.y = (SCREENH - height) / 2 + j;
      dest.w = TILEW;
      dest.h = TILEH;
      SDL_RenderCopy(renderer, img_tiles->tex, &src, &dest);
    }
  }

  snprintf(str, sizeof(str), "%d", ldata->number);
  int y = (SCREENH - img_numbers->h) / 2;
  int x = (SCREENW - (img_numbers->w / 10) * strlen(str)) / 2;
  for (i = 0; i < strlen(str); i++)
    drawimage(renderer, img_numbers, 10, 1, str[i] - '0' + 1, 1, x + (i * (img_numbers->w / 10)), y);
}
