#include <SDL.h>
#include <SDL_image.h>
#include <SDL_mixer.h>
#include <SDL_ttf.h>
#include "font.h"
#include "resource.h"
#include "path.h"

texture_t * img_title, * img_playerselect, * img_stats, * img_numbers;
texture_t * img_generators[2], * img_collectibles, * img_badguys[2];
texture_t * img_grimreaper, * img_blast;
texture_t * img_tiles, * img_check, * img_bigtitle;
texture_t * img_player[4], * img_arrows[4];
texture_t * img_instructions[4];

Mix_Chunk * snd_arrow, * snd_fire, * snd_spell, *snd_pickup, *snd_key;
Mix_Chunk * snd_eat, * snd_welcome, * snd_upgrade, *snd_bomb;
Mix_Chunk * snd_bombpickup, * snd_transition, * snd_open;
Mix_Chunk * snd_lowhealth[4];
Mix_Chunk * snd_hurt[4];

void loadimages(SDL_Renderer *renderer)
{
  char fname[1024];

  img_title = loadimage(renderer, "title.png");
  img_playerselect = loadimage(renderer, "playerselect.png");
  img_stats = loadimage(renderer, "stats.png");
  img_numbers = loadimage(renderer, "numbers.png");
  img_generators[0] = loadimage(renderer, "generators.png");
  img_generators[1] = loadimage(renderer, "generators2.png");
  img_collectibles = loadimage(renderer, "collectibles.png");
  img_badguys[0] = loadimage(renderer, "badguys.png");
  img_badguys[1] = loadimage(renderer, "badguys2.png");
  img_grimreaper = loadimage(renderer, "grimreaper.png");
  img_blast = loadimage(renderer, "eyeblast.png");
  img_tiles = loadimage(renderer, "tiles.png");
  img_check = loadimage(renderer, "check.png");
  img_bigtitle = loadimage(renderer, "bigtitle.png");
  img_player[0] = loadimage(renderer, "playera.png");
  img_arrows[0] = loadimage(renderer, "arrowsa.png");
  img_player[1] = loadimage(renderer, "playerb.png");
  img_arrows[1] = loadimage(renderer, "arrowsb.png");
  img_player[2] = loadimage(renderer, "playerc.png");
  img_arrows[2] = loadimage(renderer, "arrowsc.png");
  img_player[3] = loadimage(renderer, "playerd.png");
  img_arrows[3] = loadimage(renderer, "arrowsd.png");
  for (int i = 0; i < 4; i++)
  {
    snprintf(fname, sizeof(fname), "instructions/instruct%d.png", i+1);
    img_instructions[i] = loadimage(renderer, fname);
  }
}

void loadplayersounds(int ply, int plytype)
{
  char fname[1024];

  if (snd_lowhealth[ply])
    Mix_FreeChunk(snd_lowhealth[ply]);
  if (snd_hurt[ply])
    Mix_FreeChunk(snd_hurt[ply]);
  snprintf(fname, sizeof(fname), "lowhealth%d-%d-0.ogg", ply, plytype);
  snd_lowhealth[ply] = loadsound(fname);
  snprintf(fname, sizeof(fname), "hurt%d.ogg", plytype);
  snd_hurt[ply] = loadsound(fname);
}

void freeimages()
{
  int i;

  freeimage(img_title);
  freeimage(img_playerselect);
  freeimage(img_stats);
  freeimage(img_numbers);
  freeimage(img_collectibles);
  for (i = 0; i < 2; i++)
  {
    freeimage(img_generators[i]);
    freeimage(img_badguys[i]);
  }
  freeimage(img_blast);
  freeimage(img_tiles);
  freeimage(img_check);
  freeimage(img_bigtitle);
  for (i = 0; i < 4; i++)
  {
    freeimage(img_player[i]);
    freeimage(img_arrows[i]);
  }
  for (i = 0; i < 4; i++)
  {
    freeimage(img_instructions[i]);
  }
}

texture_t * loadimage(SDL_Renderer *renderer, char * fname)
{
  char fullfname[1024];
  SDL_Surface * tmp1;
  SDL_Texture * tmp2;
  texture_t *result;

  char *data_path = get_data_path();
  snprintf(fullfname, sizeof(fullfname), "%sdata/images/%s", data_path, fname);
  tmp1 = IMG_Load(fullfname);
  if (tmp1 == NULL)
  {
    fprintf(stderr, "Couldn't load %s: %s\n", fullfname, SDL_GetError());
    SDL_Quit();
    exit(1);
  }

  tmp2 = SDL_CreateTextureFromSurface(renderer, tmp1);
  if (tmp2 == NULL)
  {
    fprintf(stderr, "Couldn't convert %s: %s\n", fullfname, SDL_GetError());
    SDL_Quit();
    exit(1);
  }
  result = (texture_t *)malloc(sizeof(texture_t));
  result->tex = tmp2;
  result->w = tmp1->w;
  result->h = tmp1->h;
  SDL_FreeSurface(tmp1);

  return(result);
}

texture_t * createtext(SDL_Renderer *renderer, const char *text, SDL_Color c)
{
  texture_t *result;
  SDL_Surface *img = TTF_RenderUTF8_Solid(GetDefaultFont(get_data_path()), text, c);
  result = (texture_t *)malloc(sizeof(texture_t));
  result->tex = SDL_CreateTextureFromSurface(renderer, img);
  result->w = img->w;
  result->h = img->h;
  SDL_FreeSurface(img);
  return result;
}

void freeimage(texture_t * img)
{
  SDL_DestroyTexture(img->tex);
  free(img);
}

void loadsounds()
{
  snd_arrow = loadsound("arrow.ogg");
  snd_fire = loadsound("fire.ogg");
  snd_spell = loadsound("spell.ogg");
  snd_pickup = loadsound("pickup.ogg");
  snd_key = loadsound("key.ogg");
  snd_eat = loadsound("eat.ogg");
  snd_welcome = loadsound("welcome.ogg");
  snd_upgrade = loadsound("upgrade.ogg");
  snd_bomb = loadsound("bomb.ogg");
  snd_bombpickup = loadsound("bombpickup.ogg");
  snd_transition = loadsound("transition.ogg");
  snd_open = loadsound("open.ogg");
  for (int i = 0; i < 4; i++)
  {
    snd_lowhealth[i] = NULL;
    snd_hurt[i] = NULL;
  }
}

void freesounds()
{
  Mix_FreeChunk(snd_arrow);
  Mix_FreeChunk(snd_fire);
  Mix_FreeChunk(snd_spell);
  Mix_FreeChunk(snd_pickup);
  Mix_FreeChunk(snd_key);
  Mix_FreeChunk(snd_eat);
  Mix_FreeChunk(snd_welcome);
  Mix_FreeChunk(snd_upgrade);
  Mix_FreeChunk(snd_bomb);
  Mix_FreeChunk(snd_bombpickup);
  Mix_FreeChunk(snd_transition);
  Mix_FreeChunk(snd_open);
  for (int i = 0; i < 4; i++)
  {
    if (snd_lowhealth[i] != NULL)
      Mix_FreeChunk(snd_lowhealth[i]);
    if (snd_hurt[i] != NULL)
      Mix_FreeChunk(snd_hurt[i]);
  }
}

Mix_Chunk * loadsound(char * fname)
{
  Mix_Chunk * snd;
  char fullfname[1024];

  char *data_path = get_data_path();
  snprintf(fullfname, sizeof(fullfname), "%sdata/sounds/%s", data_path, fname);
  snd = Mix_LoadWAV(fullfname);
  if (snd == NULL)
  {
    fprintf(stderr, "Couldn't load %s: %s\n", fullfname, SDL_GetError());
    SDL_Quit();
    exit(1);
  }

  return(snd);
}
