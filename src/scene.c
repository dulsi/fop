#include "scene.h"

#define MAX_SCENE 6
static scene_t currentscenes[MAX_SCENE];
static scene_t nextscene;
static addorreplacescene replace = SCENE_ADD;
static int endscene = 0;

void replacedata(void *d1, void *d2)
{
  for (int i = 0; i < endscene; i++)
  {
    if (currentscenes[i].data == d1)
    {
      (*currentscenes[i].freedata)(d1);
      currentscenes[i].data = d2;
    }
  }
}

void replacescene(updatefunc u, drawfunc d, freedatafunc freedata, void *data)
{
  replace = SCENE_REPLACE;
  nextscene.upd = u;
  nextscene.drw = d;
  nextscene.freedata = freedata;
  nextscene.data = data;
}

void replacelast(updatefunc u, drawfunc d, freedatafunc freedata, void *data)
{
  replace = SCENE_REPLACELAST;
  nextscene.upd = u;
  nextscene.drw = d;
  nextscene.freedata = freedata;
  nextscene.data = data;
}

void addscene(updatefunc u, drawfunc d, freedatafunc freedata, void *data, bool runonly)
{
  currentscenes[endscene].upd = u;
  currentscenes[endscene].drw = d;
  currentscenes[endscene].freedata = freedata;
  currentscenes[endscene].data = data;
  currentscenes[endscene].runonly = runonly;
  endscene++;
}

void removescene()
{
  endscene--;
  if (currentscenes[endscene].data)
    (*currentscenes[endscene].freedata)(currentscenes[endscene].data);
}

bool update(int deltatime)
{
  bool cont = true;
  for (int i = 0; i < endscene; i++)
  {
    if ((!currentscenes[endscene - 1].runonly) || (i == endscene - 1))
    {
      bool result = (*currentscenes[i].upd)(deltatime, i == endscene - 1, currentscenes[i].data);
      if (!result)
        cont = false;
    }
  }
  if (replace == SCENE_REPLACE)
  {
    while (endscene > 0)
    {
      removescene();
    }
  }
  else if (replace == SCENE_REPLACELAST)
  {
    removescene();
  }
  if (replace != SCENE_ADD)
  {
    if (nextscene.upd != NULL)
      addscene(nextscene.upd, nextscene.drw, nextscene.freedata, nextscene.data, false);
    replace = SCENE_ADD;
  }
  return cont;
}

void draw(SDL_Renderer *renderer)
{
  for (int i = 0; i < endscene; i++)
  {
    if ((!currentscenes[endscene - 1].runonly) || (i == endscene - 1))
      (*currentscenes[i].drw)(currentscenes[i].data);
  }
  SDL_RenderPresent(renderer);
}

bool hasscene()
{
  return endscene > 0;
}
