#include <SDL.h>
#include "controller.h"
#include "game.h"
#include "resource.h"
#include "configjoystick.h"
#include "scene.h"

void *configjoystickinit(int ply, texture_t *img_configure, texture_t *img_playername)
{
  configdata_t *data = (configdata_t*)malloc(sizeof(configdata_t));
  memset(data, 0, sizeof(configdata_t));
  data->ply = ply;
  data->img_configure = img_configure;
  data->img_playername = img_playername;
  data->img_keydesc = loadkeytext();
  SDL_Color c;
  c.r = 255;
  c.g = 255;
  c.b = 255;
  c.a = 255;
  for (int i = 0; i < NUM_KEYS; i++)
  {
    if (controller_codes[ply].stick_codes[i].btn != SDL_CONTROLLER_BUTTON_INVALID)
      data->img_keyval[i] = createtext(renderer, SDL_GameControllerGetStringForButton(controller_codes[ply].stick_codes[i].btn), c);
    else if (controller_codes[ply].stick_codes[i].axis != SDL_CONTROLLER_AXIS_INVALID)
      data->img_keyval[i] = createtext(renderer, SDL_GameControllerGetStringForAxis(controller_codes[ply].stick_codes[i].axis), c);
    else
      data->img_keyval[i] = createtext(renderer, "Undefined", c);
  }
  return data;
}

void configjoystickfree(void *data)
{
  configdata_t *cdata = (configdata_t*)data;
  for (int i = 0; i < NUM_KEYS; i++)
  {
    freeimage(cdata->img_keydesc[i]);
    freeimage(cdata->img_keyval[i]);
  }
  free(cdata->img_keydesc);
  free(cdata);
}

bool configjoystickupdate(int deltatime, bool top, void *data)
{
  configdata_t *cdata = (configdata_t*)data;
  SDL_Event event;

  SDL_Color c;
  c.r = 255;
  c.g = 255;
  c.b = 255;
  c.a = 255;
  while (SDL_PollEvent(&event) > 0)
  {
    if (event.type == SDL_QUIT)
    {
      return false;
    }
    else if (cdata->where >= NUM_KEYS)
    {
      // Nothing to do.
    }
    else if ((event.type == SDL_CONTROLLERBUTTONDOWN) && (event.cbutton.which == controller_codes[cdata->ply].joy))
    {
      Uint8 button = event.cbutton.button;
      controller_codes[cdata->ply].stick_codes[cdata->where].btn = button;
      controller_codes[cdata->ply].stick_codes[cdata->where].axis = SDL_CONTROLLER_AXIS_INVALID;
      for (int j = 0; j < NUM_KEYS; j++)
      {
        if ((cdata->where != j) && controller_codes[cdata->ply].stick_codes[j].btn == button)
        {
          controller_codes[cdata->ply].stick_codes[j].btn = SDL_CONTROLLER_BUTTON_INVALID;
        }
      }
      freeimage(cdata->img_keyval[cdata->where]);
      cdata->img_keyval[cdata->where] = createtext(renderer, SDL_GameControllerGetStringForButton(button), c);
      cdata->where++;
    }
    else if ((event.type == SDL_CONTROLLERAXISMOTION) && (event.caxis.which == controller_codes[cdata->ply].joy) && (abs(event.caxis.value) > joydeadzone))
    {
      Uint8 axis = event.caxis.axis;
      int axis_sign = sign(event.caxis.value);
      bool used = false;
      for (int j = 0; j < cdata->where; j++)
      {
        if ((controller_codes[cdata->ply].stick_codes[j].axis == axis) && (controller_codes[cdata->ply].stick_codes[j].axis_sign == axis_sign))
        {
          used = true;
        }
      }
      if (!used)
      {
        controller_codes[cdata->ply].stick_codes[cdata->where].axis = axis;
        controller_codes[cdata->ply].stick_codes[cdata->where].axis_sign = axis_sign;
        controller_codes[cdata->ply].stick_codes[cdata->where].btn = SDL_CONTROLLER_BUTTON_INVALID;
        for (int j = cdata->where + 1; j < NUM_KEYS; j++)
        {
          if (controller_codes[cdata->ply].stick_codes[j].axis == axis && controller_codes[cdata->ply].stick_codes[j].axis_sign == axis_sign)
          {
            controller_codes[cdata->ply].stick_codes[j].btn = SDL_CONTROLLER_AXIS_INVALID;
          }
        }
        freeimage(cdata->img_keyval[cdata->where]);
        cdata->img_keyval[cdata->where] = createtext(renderer, SDL_GameControllerGetStringForAxis(controller_codes[cdata->ply].stick_codes[cdata->where].axis), c);
        cdata->where++;
      }
    }
  }
  if (cdata->where >= NUM_KEYS)
    removescene();
  return true;
}
