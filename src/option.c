#include <SDL.h>
#include "controller.h"
#include "game.h"
#include "resource.h"
#include "option.h"
#include "scene.h"
#include "configkey.h"
#include "configjoystick.h"

void *optioninit()
{
  int i;
  optiondata_t *data = (optiondata_t*)malloc(sizeof(optiondata_t));
  memset(data, 0, sizeof(optiondata_t));
  const char *option_text[11] = {"Options", "Fullscreen", "Yes", "No", "Configure", "Player 1", "Player 2", "Player 3", "Player 4", "Keyboard", "Joystick"};
  SDL_Color c;
  c.r = 255;
  c.g = 255;
  c.b = 255;
  c.a = 255;
  for (i = 0; i < 11; i++)
  {
    data->img_options[i] = createtext(renderer, option_text[i], c);
  }
  for (i = 0 ; i < 4; i++)
  {
    data->prevfire[i] = 1;
    data->prevup[i] = 0;
    data->prevdown[i] = 0;
  }
  return data;
}

void optionfree(void *data)
{
  optiondata_t *odata = (optiondata_t*)data;
  free(odata);
  writeconfig();
}

bool optionupdate(int deltatime, bool top, void *data)
{
  int i;
  optiondata_t *odata = (optiondata_t*)data;

  int result = processevent(controllers, NULL);
  if (result == PROCESS_QUIT)
    return false;
  if (result == PROCESS_BACK)
  {
    replacescene(NULL, NULL, NULL, NULL);
    return true;
  }
  for (int i = 0; i < 4; i++)
  {
    if (controllers[i].key_fire && controllers[i].key_fire != odata->prevfire[i])
    {
      if (odata->option == 0)
      {
        fullscreen = !fullscreen;
        SDL_SetWindowFullscreen(screen, (fullscreen ? SDL_WINDOW_FULLSCREEN_DESKTOP : 0));
      }
      else if (odata->option % 2 == 1)
      {
        addscene(configkeyupdate, configdraw, configkeyfree, configkeyinit(odata->option / 2, odata->img_options[4], odata->img_options[5 + odata->option / 2]), true);
      }
      else
      {
        addscene(configjoystickupdate, configdraw, configjoystickfree, configjoystickinit(odata->option / 2 - 1, odata->img_options[4], odata->img_options[4 + odata->option / 2]), true);
      }
    }
    if (controllers[i].key_bomb)
    {
      replacescene(NULL, NULL, NULL, NULL);
    }
    if (controllers[i].key_up && controllers[i].key_up != odata->prevup[i])
    {
      odata->option--;
      if (odata->option < 0)
        odata->option = 8;
    }
    if (controllers[i].key_down && controllers[i].key_down != odata->prevdown[i])
    {
      odata->option++;
      if (odata->option > 8)
        odata->option = 0;
    }
    odata->prevfire[i] = controllers[i].key_fire;
    odata->prevup[i] = controllers[i].key_up;
    odata->prevdown[i] = controllers[i].key_down;
  }
  return true;
}

void optiondraw(void *data)
{
  int i;
  SDL_Rect dest, src;
  optiondata_t *odata = (optiondata_t*)data;

  SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
  SDL_RenderClear(renderer);
  dest.x = (SCREENW - odata->img_options[0]->w) / 2;
  dest.y = 0;
  dest.w = odata->img_options[0]->w;
  dest.h = odata->img_options[0]->h;
  SDL_RenderCopy(renderer, odata->img_options[0]->tex, NULL, &dest);
  dest.x = 150;
  dest.y = 40;
  dest.w = odata->img_options[1]->w;
  dest.h = odata->img_options[1]->h;
  SDL_RenderCopy(renderer, odata->img_options[1]->tex, NULL, &dest);
  dest.x = 400;
  if (fullscreen)
  {
    dest.w = odata->img_options[2]->w;
    dest.h = odata->img_options[2]->h;
    SDL_RenderCopy(renderer, odata->img_options[2]->tex, NULL, &dest);
  }
  else
  {
    dest.w = odata->img_options[3]->w;
    dest.h = odata->img_options[3]->h;
    SDL_RenderCopy(renderer, odata->img_options[3]->tex, NULL, &dest);
  }
  if (odata->option == 0)
  {
    src.x = 64;
    src.y = 32;
    src.w = TILEW;
    src.h = TILEH;
    dest.x = 360;
    dest.w = TILEW;
    dest.h = TILEH;
    SDL_RenderCopy(renderer, img_arrows[2]->tex, &src, &dest);
  }
  dest.x = (SCREENW - odata->img_options[4]->w) / 2;
  dest.y = 80;
  dest.w = odata->img_options[4]->w;
  dest.h = odata->img_options[4]->h;
  SDL_RenderCopy(renderer, odata->img_options[4]->tex, NULL, &dest);
  for (i = 0; i < 4; i++)
  {
    dest.x = 150;
    dest.y += 40;
    dest.w = odata->img_options[5 + i]->w;
    dest.h = odata->img_options[5 + i]->h;
    SDL_RenderCopy(renderer, odata->img_options[5 + i]->tex, NULL, &dest);
    dest.x = 400;
    dest.w = odata->img_options[9]->w;
    dest.h = odata->img_options[9]->h;
    SDL_RenderCopy(renderer, odata->img_options[9]->tex, NULL, &dest);
    if (odata->option == i * 2 + 1)
    {
      src.x = 64;
      src.y = 32;
      src.w = TILEW;
      src.h = TILEH;
      dest.x = 360;
      dest.w = TILEW;
      dest.h = TILEH;
      SDL_RenderCopy(renderer, img_arrows[2]->tex, &src, &dest);
    }
    dest.y += 40;
    dest.x = 400;
    dest.w = odata->img_options[10]->w;
    dest.h = odata->img_options[10]->h;
    SDL_RenderCopy(renderer, odata->img_options[10]->tex, NULL, &dest);
    if (odata->option == i * 2 + 2)
    {
      src.x = 64;
      src.y = 32;
      src.w = TILEW;
      src.h = TILEH;
      dest.x = 360;
      dest.w = TILEW;
      dest.h = TILEH;
      SDL_RenderCopy(renderer, img_arrows[2]->tex, &src, &dest);
    }
  }
}
