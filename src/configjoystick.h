#ifndef __CONFIGJOYSTICK_H
#define __CONFIGJOYSTICK_H

#include "configkey.h"

void *configjoystickinit(int ply, texture_t *img_configure, texture_t *img_playername);
void configjoystickfree(void *data);
bool configjoystickupdate(int deltatime, bool top, void *data);

#endif
