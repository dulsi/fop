#ifndef __ABOUT_H
#define __ABOUT_H

typedef struct aboutdata_s {
  texture_t *img_about;
  int prevfire[4];
} aboutdata_t;

void *aboutinit();
void aboutfree(void *data);
bool aboutupdate(int deltatime, bool top, void *data);
void aboutdraw(void *data);

#endif
