#ifndef __MENU_H
#define __MENU_H

#include "scene.h"

typedef void *(*sceneinitfunc)();

typedef struct menuitem_s {
  texture_t *img_text;
  updatefunc upd;
  drawfunc drw;
  freedatafunc freedata;
  sceneinitfunc sceneinit;
  addorreplacescene state;
} menuitem_t;

typedef struct menudata_s {
  int option;
  int enditem;
  int maxw;
  menuitem_t menuitem[20];
} menudata_t;

menudata_t *menuinit();
void menuadd(menudata_t *data, const char *text, updatefunc u, drawfunc d, freedatafunc freedata, sceneinitfunc sceneinit, addorreplacescene state);
void menufree(void *data);
bool menuupdate(int deltatime, bool top, void *data);
void menudraw(void *data);

#endif
