#include <SDL.h>
#include "controller.h"
#include "game.h"
#include "resource.h"
#include "selectlist.h"

selectdata_t *selectinit()
{
  selectdata_t *data = (selectdata_t*)malloc(sizeof(selectdata_t));
  memset(data, 0, sizeof(selectdata_t));
  return data;
}

void selectadd(selectdata_t *data, const char *text, selectchoicefunc choicefunc)
{
  SDL_Color c;
  c.r = 0;
  c.g = 0;
  c.b = 0;
  c.a = 255;
  data->selectitem[data->enditem].img_text = createtext(renderer, text, c);
  data->selectitem[data->enditem].choicefunc = choicefunc;
  if (data->maxw < data->selectitem[data->enditem].img_text->w)
    data->maxw = data->selectitem[data->enditem].img_text->w;
  data->enditem++;
}

void selectfree(void *data)
{
  selectdata_t *sdata = (selectdata_t*)data;
  for (int i = 0; i < sdata->enditem; i++)
    freeimage(sdata->selectitem[i].img_text);
  free(sdata);
}

bool selectupdate(int deltatime, bool top, void *data)
{
  int oldkey_up[4], oldkey_down[4], oldkey_fire[4];
  int i;
  selectdata_t *sdata = (selectdata_t*)data;
  for (i = 0; i < 4; i++)
  {
    oldkey_fire[i] = controllers[i].key_fire;
    oldkey_up[i] = controllers[i].key_up;
    oldkey_down[i] = controllers[i].key_down;
  }
  int result = processevent(controllers, NULL);
  if (result == PROCESS_QUIT)
    return false;
  if (result == PROCESS_BACK)
  {
    replacescene(NULL, NULL, NULL, NULL);
    return true;
  }
  for (i = 0; i < 4; i++)
  {
    if (controllers[i].key_down && controllers[i].key_down != oldkey_down[i])
    {
      sdata->option++;
      if (sdata->option >= sdata->enditem)
      {
        sdata->option = 0;
        sdata->top = 0;
      }
      else if (sdata->option > sdata->top + 9)
        sdata->top = sdata->option - 9;
    }
    if (controllers[i].key_up && controllers[i].key_up != oldkey_up[i])
    {
      sdata->option--;
      if (sdata->option < 0)
      {
        sdata->option = sdata->enditem - 1;
        sdata->top = sdata->option - 9;
        if (sdata->top < 0)
          sdata->top = 0;
      }
      else if (sdata->top > sdata->option)
        sdata->top = sdata->option;
    }
    if (controllers[i].key_fire && controllers[i].key_fire != oldkey_fire[i])
    {
      sdata->selectitem[sdata->option].choicefunc(sdata->option);
      replacelast(NULL, NULL, NULL, NULL);
    }
    if (controllers[i].key_bomb)
    {
      replacelast(NULL, NULL, NULL, NULL);
    }
  }
  return true;
}

void selectdraw(void *data)
{
  SDL_Rect src, dest;
  int i, j;
  selectdata_t *sdata = (selectdata_t*)data;

  int width = sdata->maxw + 25 + TILEW;
  width = (width / TILEW) * TILEW + (width % TILEW ? TILEW : 0);
  int height = (sdata->enditem < 10 ? sdata->enditem : 10) * 40 + 20;
  height = (height / TILEH) * TILEH + (height % TILEH ? TILEH : 0);

  for (i = 0; i < width; i += TILEW)
  {
    for (j = 0; j < height; j += TILEH)
    {
      if (i == 0 && j == 0)
        src.x = (TILEW + 2) * 5;
      else if (i == width - TILEW && j == 0)
        src.x = (TILEW + 2) * 7;
      else if (j == 0)
        src.x = (TILEW + 2) * 6;
      else if (i == 0 && j == height - TILEH)
        src.x = (TILEW + 2) * 11;
      else if (i == width - TILEW && j == height - TILEH)
        src.x = (TILEW + 2) * 13;
      else if (i == 0)
        src.x = (TILEW + 2) * 8;
      else if (i == width - TILEW)
        src.x = (TILEW + 2) * 10;
      else if (j == height - TILEH)
        src.x = (TILEW + 2) * 12;
      else
        src.x = (TILEW + 2) * 9;
      src.y = (TILEH + 2) * (8);
      src.w = TILEW;
      src.h = TILEH;
      dest.x = (SCREENW - width) / 2 + i;
      dest.y = (SCREENH - height) / 2 + j;
      dest.w = TILEW;
      dest.h = TILEH;
      SDL_RenderCopy(renderer, img_tiles->tex, &src, &dest);
    }
  }

  src.x = 0;
  src.y = 0;
  dest.x = (SCREENW - width) / 2 + 45;
  dest.y = (SCREENH - height) / 2 + 10;
  for (int i = 0; i + sdata->top < sdata->enditem && i < 10; i++)
  {
    src.w = sdata->selectitem[i + sdata->top].img_text->w;
    src.h = sdata->selectitem[i + sdata->top].img_text->h;

    dest.w = sdata->selectitem[i + sdata->top].img_text->w;
    dest.h = sdata->selectitem[i + sdata->top].img_text->h;

    SDL_RenderCopy(renderer, sdata->selectitem[i + sdata->top].img_text->tex, &src, &dest);
    if (sdata->option == i + sdata->top)
    {
      src.x = 64;
      src.y = 32;
      src.w = TILEW;
      src.h = TILEH;
      dest.x = dest.x - 5 - TILEW;
      dest.w = TILEW;
      dest.h = TILEH;
      SDL_RenderCopy(renderer, img_arrows[2]->tex, &src, &dest);
      src.x = 0;
      src.y = 0;
      dest.x = dest.x + 5 + TILEW;
    }
    dest.y += 40;
  }
  if (sdata->top != 0)
  {
    src.x = 0;
    src.y = (TILEH + 2) * (9);
    dest.x = (SCREENW - width) / 2 + width - TILEW - 10;
    dest.y = (SCREENH - height) / 2 + 10;
    src.w = TILEW;
    src.h = TILEH;
    dest.w = TILEW;
    dest.h = TILEH;
    SDL_RenderCopy(renderer, img_tiles->tex, &src, &dest);
  }
  if (sdata->top + 10 != sdata->enditem)
  {
    src.x = TILEW + 2;
    src.y = (TILEH + 2) * (9);
    dest.x = (SCREENW - width) / 2 + width - TILEW - 10;
    dest.y = (SCREENH - height) / 2 + 10 + 9 * 40;
    src.w = TILEW;
    src.h = TILEH;
    dest.w = TILEW;
    dest.h = TILEH;
    SDL_RenderCopy(renderer, img_tiles->tex, &src, &dest);
  }
}
