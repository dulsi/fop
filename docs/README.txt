Fight or Perish
https://dulsi.itch.io/fight-or-perish/

About:
------
Fight or Perish is a multi-player dungeon crawling game,
based on the game concept in Jack Pavelich's Atari 8-bit
computer game "Dandy" (aka his "Thesis of Terror" at MIT),
from 1983.

The game concept is well-known thanks to Atari Games'
"Gauntlet", which was obviously influenced by "Dandy".
Some concepts from Gauntlet were also borrowed.

The name Fight or Perish was chosen so that the abbreviation
is FOP which is a play on the term "Dandy" (itself being a
play on "D&D", or "Dungeons and Dragons").


Gameplay Overview:
------------------
Up to four players move their characters around a dungeon,
collecting treasure and other items, while avoiding being
killed by monsters.

Each player is armed with a weapon, which they can shoot
in any of 8 directions.  Players can also fight 'hand-to-hand'
against enemies, although this incurs damage to the player's
character.

Each dungeon level begins with various enemies, treasure
and other collectible items placed on it.
This includes 'generators': places in the dungeon that
give rise to new enemies.  Because of this, the more time
you spend in the dungeon, the more enemies there might be.

Some enemies require multiple hits to be destroyed.
Generators that create those enemies also require multiple hits.
(Each hit to a generator causes it to create enemies that
require one less hit.)


Starting a Game:
----------------
From the main menu, press Start button to start or select start
from the menu.

On the new game screen, press Start button to activate a player.
Each player presses their own Left/Right controls to
change which character they will play as. (See "Controls".)
Each active player's character stats will be displayed
below their character:  Health, Weapon and Speed.
(See "Characters".) Press Fire button to select you character.

Press Up or Down to change the level. Once one player has
selected his character you cannot alter the level anymore.
FIXME: It goes from 1-99, regardless of how many
level maps actually exist.



Characters:
-----------
There are four characters that each player can choose from,
each with different statistics:

* "Fairy"
  Speed:  Fast   (3)
  Health: Normal (2)
  Weapon: Weak   (1) (Cannot shoot diagonal)

* "Komodo"
  Speed:  Slow   (1)
  Health: High   (3)
  Weapon: Normal (2)

* "Archer"
  Speed:  Normal (2)
  Health: Normal (2)
  Weapon: Normal (2)

* "Sorcerer"
  Speed:  Normal (2)
  Health: Low    (1)
  Weapon: Strong (3) (Not absorbed when destroying an enemy)


Collectible Items:
------------------
* Treasure
  This adds to your score.
  When you pass certain score thresholds, your health is raised
  to its current maximum level and, if possible, the maximum
  level itself is increased.

* Key
  Having one or more keys allows you to open a door.
  For each door you open, one of your keys is used.

* Bomb
  This allows the player to instantly damage all enemies
  and generators currently visible on the screen
  (reducing their power, or destroying them if they're already
  at the lowest level).

  You can either collect the bomb, and use it later (see 'Controls'),
  or shoot it with your weapon to activate it immediately.

* Pile (looks like treasure and keys)
  When a player dies, any collectible objects (keys and bombs)
  that they were carrying are placed in a pile on the map
  (at the spot where they died).  It's important to gather
  this, because that player may have had the keys required
  to exit the current dungeon level.

* Food
  Collecting food increases your health.  (It cannot go beyond
  its current maximum level, though.)

  Note: When food is hit by a weapon, it gets destroyed...
  so watch where you're shooting!

* Temporary Upgrade (a crate with a "?" on the side)
  Collecting a temporary upgrade will improve one of your
  abilities for a limited amount of time.  The ability is
  picked randomly, and if you're already at the limit,
  the upgrade does nothing.
  FIXME: Should it never give out useless bonuses?

  A countdown meter appears next to your other status
  information.  An icon from the player select screen
  appears, denoting which ability is being improved.

  * Health - You are temporarily impervious to attack.
  * Speed - You move more quickly.
  * Weapon - Your weapon is more powerful.


General Controls:
-----------------
Press player's fire button to select menu items. Start button
immediately start the game.

Press [Esc] from the game start screen, or within the
game to quit back to the main menu.


Default player controls:
------------------------
Player 1:
  Up:        [Up arrow]
  Down:      [Down arrow]
  Left:      [Left arrow]
  Right:     [Right arrow]
  Fire:      [Ctrl (on right)]
  Bomb:      [/]
  Start:     [1]

Player 2:
  Up:        [W]
  Down:      [S]
  Left:      [A]
  Right:     [D]
  Fire:      [Q]
  Bomb:      [E]
  Start:     [2]

Player 3:
  Up:        [Keypad 8]
  Down:      [Keypad 2]
  Left:      [Keypad 4]
  Right:     [Keypad 6]
  Fire:      [Keypad 0]
  Bomb:      [Keypad +]
  Start:     [3]

Player 4:
  Up:        [T]
  Down:      [G]
  Left:      [F]
  Right:     [H]
  Fire:      [R]
  Bomb:      [Y]
  Start:     [4]


Overriding player controls:
---------------------------

In FOP's configuration file ("~/.local/share/fightorperish/config" on Linux),
controls for each player may be stored. You can config the settings from the
options menu.

The control setting lines begin with:

  P#_FIRE = 
  P#_BOMB =
  P#_START =
  P#_UP =
  P#_DOWN =
  P#_LEFT =
  P#_RIGHT =

... where "#" is a number between 1 and 4,
corresponding to the player.

For joystick controls, use the value:

  JOY_BTN#
  JOY_AXIS#_POS
  JOY_AXIS#_NEG

Where '#' is the number of the button or axis on the
controller.

For example, to use a PlayStation controller
attached to a USB connector for player 1,
with the D-pad for movement, (X) for fire and
(O) for bomb, use:

  P1_DOWN = JOY_BTN12
  P1_UP = JOY_BTN11
  P1_LEFT = JOY_BTN13
  P1_RIGHT = JOY_BTN14
  P1_FIRE = JOY_BTN0
  P1_BOMB = JOY_BTN1
  P1_START = JOY_BTN6


Creating maps:
--------------
Map file begins with two lines with numbers.
The first describes the map width, the second the map height.

They should be followed by the map -- "map height" lines,
each "map width" long.

Map characters:

    - Empty space
  # - Wall
  = - Door segment
  % - Exit to next level

  $ - Treasure
  f - Food
  b - Bomb
  k - Key
  ? - Temporary upgrade

  A - Level 1 enemy
  B - Level 2 enemy
  C - Level 3 enemy

  1 - Level 1 generator
  2 - Level 2 generator
  3 - Level 3 generator

  5 - Starting position for player 1
  6 - Starting position for player 2
  7 - Starting position for player 3
  8 - Starting position for player 4


Credits:
--------
Programmer/Original Graphics: Bill Kendrick <bill@newbreedsoftware.com>
Programmer/Updated Graphics: Dennis Payne

data/images/tiles.png
Gauntlet-like Tiles by surt
https://opengameart.org/content/gauntlet-like-tiles
License: CC0

arrow.ogg
Bow & Arrow Shot by dorkster
https://opengameart.org/content/bow-arrow-shot
License: CC-BY-SA 3.0

fire.ogg
spell.ogg
Fire & Evil Spell by artisticdude
https://opengameart.org/content/fire-evil-spell
License: CC-BY-SA 3.0

pickup.ogg
Pickup/plastic Sound by Vinrax
https://opengameart.org/content/pickupplastic-sound
License: CC-BY 3.0

key.ogg
Key pickup by Vinrax
https://opengameart.org/content/key-pickup
License: CC-BY 3.0

eat.ogg
7 Eating Crunches by StarNinjas
https://opengameart.org/content/7-eating-crunches
License: CC0

welcome.ogg
lowheath*.ogg
Created with mimic using voice rms

upgrade.ogg
Life Pickup (Yo Frankie!) by Blender Foundation
https://opengameart.org/content/life-pickup-yo-frankie
License: CC-BY 3.0

bomb.ogg
bomb_explosion_8bit by Luke.RUSTLTD
https://opengameart.org/content/bombexplosion8bit
License: CC0

bombpickup.ogg
transition.ogg
7 Assorted Sound Effects (Menu, Level Up) by Joth
https://opengameart.org/content/7-assorted-sound-effects-menu-level-up
License: CC0

open.ogg
Plastic on bongos by remaxim
https://opengameart.org/content/plastic-bongos
License: CC-BY-SA 3.0

hurt1.ogg
hurt2.ogg
hurt3.ogg
Man Hurt Sound Effect by Aharabada
https://opengameart.org/content/man-hurt-sound-effect
License: CC0

hurt0.ogg
Female Hurt Grunts & Groans by Nocturnal_Vanguard
https://opengameart.org/content/female-hurt-grunts-groans
License: CC0

LiberationSans-Bold.ttf
License: OFL
